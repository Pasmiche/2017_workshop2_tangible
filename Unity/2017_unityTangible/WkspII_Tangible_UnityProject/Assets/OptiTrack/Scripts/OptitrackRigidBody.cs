﻿//======================================================================================================
// Copyright 2016, NaturalPoint Inc.
//======================================================================================================

using System;
using UnityEngine;


public class OptitrackRigidBody : MonoBehaviour
{
    public OptitrackStreamingClient StreamingClient;
    public Int32 RigidBodyId;
    public OptitrackCalibration _calib;

    [Header("Offset Config")]
    public float _xOffset;
    public float _yOffset;
    public float _zOffset;

    public float _xScale;
    public float _yScale;
    public float _zScale;


    void Start()
    {
        // If the user didn't explicitly associate a client, find a suitable default.
        if ( this.StreamingClient == null )
        {
            this.StreamingClient = OptitrackStreamingClient.FindDefaultClient();

            // If we still couldn't find one, disable this component.
            if ( this.StreamingClient == null )
            {
                Debug.LogError( GetType().FullName + ": Streaming client not set, and no " + typeof( OptitrackStreamingClient ).FullName + " components found in scene; disabling this component.", this );
                this.enabled = false;
                return;
            }
        }
    }


#if UNITY_2017_1_OR_NEWER
    void OnEnable()
    {
        Application.onBeforeRender += OnBeforeRender;
    }


    void OnDisable()
    {
        Application.onBeforeRender -= OnBeforeRender;
    }


    void OnBeforeRender()
    {
        UpdatePose();
    }
#endif


    void Update()
    {
        UpdatePose();
    }


    void UpdatePose()
    {
        OptitrackRigidBodyState rbState = StreamingClient.GetLatestRigidBodyState( RigidBodyId );
        if ( rbState != null )
        {
            Vector3 scaleMod = _calib.GetScale();
            Vector3 posOffset = _calib.GetOffset();
            float xPos = rbState.Pose.Position.x *  -scaleMod.x + posOffset.x;
            float yPos = rbState.Pose.Position.y *  -scaleMod.y + posOffset.y;
            float zPos = rbState.Pose.Position.z *  -scaleMod.z + posOffset.z;

            this.transform.position = new Vector3(xPos, yPos, zPos);
            this.transform.rotation = rbState.Pose.Orientation;
        }
    }
}
