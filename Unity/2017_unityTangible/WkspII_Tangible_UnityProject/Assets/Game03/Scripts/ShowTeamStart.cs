﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowTeamStart : MonoBehaviour {

	public AnimationCurve _curve;
	public Text _text;

	void Start()
	{
		transform.localScale = Vector3.zero;
	}

	public void ShowFirstTeam(string teamName, Color teamColor, float duration)
	{
		_text.enabled = true;
		_text.text = teamName.ToUpper() +"\n" + "commencent!";
		_text.color = teamColor;
		StartCoroutine(ShowFirstTeamCoroutine(duration));
	}

	IEnumerator ShowFirstTeamCoroutine(float duration)
	{
		for(float t = 0; t< 1; t+= Time.deltaTime / duration)
		{
			float f = _curve.Evaluate(t);
			transform.localScale = new Vector3(f,f,f);
			yield return null;
		}

		transform.localScale = Vector3.one;
		yield return new WaitForSeconds(duration * 2);

		for(float t = 0; t< 1; t+= Time.deltaTime / duration)
		{
			transform.localScale = Vector3.Lerp(Vector3.one, Vector3.zero,  _curve.Evaluate(t));
			yield return null;
		}
		transform.localScale = Vector3.zero;
		_text.enabled = false;
	}
}
