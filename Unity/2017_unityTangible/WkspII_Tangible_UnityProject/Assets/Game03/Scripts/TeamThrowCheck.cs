﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TeamThrowCheck : MonoBehaviour 
{

	public Asteroid[] _asteroids;
	public OrbitGameManager.teamAssignment _actualTeam;
	public Image _teamTurnIndicator;
	public LightCue_Manager _lightManager;
	public AudioSource _audioSource;

	
	void OnTriggerEnter(Collider other)
	{
		if(other.name.Contains("Asteroid"))
		{
			print("Enter");
			OrbitGameManager.Instance.SetTeamThrow(other.GetComponent<Asteroid>()._team);
		}
	}

	public void NextTeamCheck()
	{
		print("NEXT TEAM CHECK!");
		for(int i =  0; i< _asteroids.Length; i++)
		{
			if(!_asteroids[i]._readyToThrow)
			{
				return;
			}
		}

		OrbitGameManager.teamAssignment team = _asteroids[0]._team;
		for(int i =  0; i< _asteroids.Length; i++)
		{
			if(_asteroids[i]._team != team)
			{
				//print("Not all the same color");
				return;
			}
		}

		if(_actualTeam != team)
		{
			//print("Changing to " + team.ToString() + " team");
			Color col = OrbitGameManager.Instance._teamLooks[(int)team - 1]._mainColor;
			_teamTurnIndicator.color = col;
			_lightManager.NextTeamLight(col);
			_audioSource.Play();
		}
	}

}
