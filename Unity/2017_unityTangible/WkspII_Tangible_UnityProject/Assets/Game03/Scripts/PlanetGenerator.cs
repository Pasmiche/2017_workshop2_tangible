﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetGenerator : MonoBehaviour {

	public GameObject _planetPrefab;
	public OrbitGameManager _gameManager;
	public int _planetsPerTeam;

	void Start()
	{
		//_gameManager = OrbitGameManager.Instance;
		//CreatePlanets(_planetsPerTeam);
	}

	public void Initialize(OrbitGameManager gameManager)
	{
		_gameManager = gameManager;
		CreatePlanets(_planetsPerTeam);
	}

	void CreatePlanets(int planetsPerTeam)
	{
		StartCoroutine(CreatePlanetsCoroutine(planetsPerTeam));
	}

	IEnumerator CreatePlanetsCoroutine(int quantityPerTeam)
	{
		List<Planet> planets = new List<Planet>();
		float angleBetween =  360.0f / ((float)quantityPerTeam * (float)2);
		float angle = 0;
		int index = 0;
		for(int i = 0; i < quantityPerTeam; i++)
		{
			GameObject planetBlue = Instantiate (_planetPrefab, Vector3.zero, Quaternion.identity);
			
			planetBlue.transform.eulerAngles = new Vector3(0, angle, 0);
			Planet scriptBlue = planetBlue.transform.GetChild(0).GetComponent<Planet>();
			scriptBlue.Init(OrbitGameManager.teamAssignment.one, "Orbit1", _gameManager._orbits[0], _gameManager._teamLooks[0]._ballPointValuePrefab);
			planets.Add(scriptBlue);
			index += 1;
			angle += angleBetween * 2;// * index;
			yield return new WaitForSeconds (0.05f);

			
		}
		angle += angleBetween;
		for(int i = 0; i < quantityPerTeam; i++)
		{

			GameObject planetRed = Instantiate (_planetPrefab, Vector3.zero, Quaternion.identity);
			planetRed.transform.eulerAngles = new Vector3(0, angle, 0);
			Planet scriptRed = planetRed.transform.GetChild(0).GetComponent<Planet>();
			scriptRed.Init(OrbitGameManager.teamAssignment.two, "Orbit1", _gameManager._orbits[0], _gameManager._teamLooks[1]._ballPointValuePrefab);
			planets.Add(scriptRed);
			index += 1;
			angle += angleBetween * 2;// * index;
			yield return new WaitForSeconds (0.05f);
		}

		//yield return new WaitForSeconds(1);

		_gameManager._planets = planets;
		for(int i = 0; i< planets.Count; i++)
		{
			planets[i].GetComponent<Planet>().PlanetReady();
		}
	}
}
