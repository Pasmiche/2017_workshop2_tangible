﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gamekeeper : MonoBehaviour 
{
	[Serializable]
	public class GameMessage
	{
		public string type;
		public string _namespace;
		public long timestamp;
		public Data data;
		public string name;
		public string action;

		public string PlayerName(int player)
		{
			return data.participants[player].participant_name;
		}

		public string PlayerId(int player)
		{
			return data.participants[player].participant_id;
		}
	}

	[Serializable]
	public class Data
	{
		public string game_id;
		public string session_id;
		public Participant[] participants;
	}

	[Serializable]
	public class Participant
	{
		public string participant_id;
		public string participant_name;
		public string participant_family;
		public int participant_score;
	}

}
