﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orbit : MonoBehaviour {

	[Header ("Orbit Parameters")]
	public float _rotationSpeed;
	//public bool _isCenter;
	//public bool _isOuter;

	[Header ("Colliders")]
	public bool _colliderSetup;
	public GameObject _colliderPrefab;
	public GameObject[] _colliderParents;
	public float _colliderAngle;
	public int _colliderCount;
	public int _layer;

	float _colliderTotalAngle;
	

	[Header ("Orbit Rendering")]
	public LineRenderer _lineRenderer;
	public int _resolution;
	public float _radius;

	float _oldRadius;
	float _oldResolution;


	void Start () 
	{
		ManageColliders();
		CreateOrbit();
	}

	void CreateOrbit()
	{
		_lineRenderer.positionCount = _resolution;
		_lineRenderer.useWorldSpace = true;

		float angle = 0;
		for(int i = 0; i< _resolution; i++)
		{
			float x = Mathf.Sin (Mathf.Deg2Rad * angle) * _radius;
            float y = Mathf.Cos (Mathf.Deg2Rad * angle) * _radius;

			_lineRenderer.SetPosition(i,new Vector3(x, -0.5f, y));
			angle += 360 / _resolution;
		}
	}
	
	void Update()
	{	
		//transform.Rotate(Vector3.up, _rotationSpeed);
		if(_colliderSetup)
		{
			ManageColliders();
		}

		if(_oldRadius != _radius)
		{
			CreateOrbit();
		}

		if(_oldResolution != _resolution)
		{
			CreateOrbit();
		}

		_oldRadius = _radius;
		_oldResolution = _resolution;
	}

	void ManageColliders()
	{
		if(_colliderCount != _colliderParents.Length)
		{
			RespawnColliders();
		}
		
		_colliderTotalAngle = 0;
		for(int i = 0; i< _colliderParents.Length; i++)
		{
			_colliderParents[i].transform.localEulerAngles = new Vector3 (0,_colliderTotalAngle,0);
			_colliderTotalAngle += _colliderAngle;
		}
	}

	void RespawnColliders()
	{
		foreach(GameObject go in _colliderParents)
		{
			Destroy(go);
		}
		_colliderParents = new GameObject[_colliderCount] ;
		for(int i = 0; i< _colliderParents.Length; i++)
		{
			_colliderParents[i] = Instantiate(_colliderPrefab, transform.position, Quaternion.identity, transform);
			_colliderParents[i].transform.GetChild(0).localPosition = new Vector3(0,0,_radius);
			_colliderParents[i].transform.GetChild(0).gameObject.layer = _layer;
		}
	}
}
