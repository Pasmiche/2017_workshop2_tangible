﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AsteroidStateUi : MonoBehaviour {

	public Asteroid _asteroid;
	public Image _image;
	// Use this for initialization
	
	void Update () 
	{
		_image.color = _asteroid.GetColor();
	}
}
