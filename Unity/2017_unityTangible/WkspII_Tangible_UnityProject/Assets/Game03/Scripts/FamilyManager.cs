﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FamilyManager : MonoBehaviour 
{

	public enum FamilyNames {chaba, kemish, hatahu, saduki};

	public FamilyLook[] _familyLooks;

	public Color GetTeamColor(int familyId)
	{
		return _familyLooks[familyId]._mainColor;
	}

	public FamilyLook GetFamilyLookById(int familyId)
	{
		return _familyLooks[familyId];
	}

	public FamilyLook GetFamilyLookByName(FamilyNames familyName)
	{
		print("GetFamilyLookByName: " + (int)familyName);
		return _familyLooks[(int)familyName];
	}
}

[Serializable]
public class FamilyLook
{
	public FamilyManager.FamilyNames _nameEnum;
	public string _familyName;
	public Color _mainColor;
	public Color _ledColor;
	public Texture2D _miniature;
	public Texture2D _normalMap;
	public Sprite _miniatureSprite;
	public GameObject _ballPointValuePrefab;
}
