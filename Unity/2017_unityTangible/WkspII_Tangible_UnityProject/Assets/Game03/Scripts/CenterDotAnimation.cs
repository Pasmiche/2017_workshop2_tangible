﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CenterDotAnimation : MonoBehaviour {

	public AnimationCurve _animCurve;
	public float _duration;

	Vector3 _baseScale;


	void Start()
	{
		_baseScale = transform.localScale;
	}

	void Update()
	{
		transform.localScale = _baseScale * _animCurve.Evaluate(Time.time / _duration);
	}
}
