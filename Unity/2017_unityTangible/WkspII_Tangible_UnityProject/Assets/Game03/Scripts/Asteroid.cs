﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : Collidable {

	public AttractorState _stateManager;
	public AsteroidFeedback _feedback;
	public Collider _collider;
	public Color _inactiveColor;
	public bool _readyToThrow;
	public TeamThrowCheck _throwCheck;
	Renderer _renderer;
	TrailRenderer _trail;

	OrbitGameManager _gameManager;

	bool _init;

	void Start()
	{
		_gameManager = OrbitGameManager.Instance;
		_trail = GetComponent<TrailRenderer>();
		_trail.enabled = false;
		_renderer = GetComponent<Renderer>();
		Material mat = Instantiate(_renderer.material);
		mat.name = "newMat";
		_renderer.material = mat;
	}

	public void Init(OrbitGameManager.teamAssignment team)
	{
		_team = team;
		ChangeColor();
		_trail.enabled = true;
		_init = true;
		_throwCheck.NextTeamCheck();
	}

	void Update()
	{
		if(_readyToThrow)
		{
			if(_stateManager._inBound && _stateManager._isAtRest && _stateManager._isGrounded)
			{
				if(_feedback != null)
				{
					_feedback.Deactivate();
				}
				_collider.enabled = false;
				_readyToThrow = false;
				_trail.enabled = false;
			}
		}

		if(!_readyToThrow)
		{
			_renderer.sharedMaterial.color = _inactiveColor;
			_renderer.sharedMaterial.SetColor("_EmissionColor", _inactiveColor);
			if(!_stateManager._inBound)
			{
				_readyToThrow = true;
				_collider.enabled = true;
				_trail.enabled = true;
				SwitchTeam();
				_throwCheck.NextTeamCheck();
			}
		}
	}

	public void RemoteSwitchTeam()
	{
		SwitchTeam();
	}

	void SwitchTeam()
	{
		if((int)_team == 1)
		{
			_team = (OrbitGameManager.teamAssignment)2;
		}
		else
		{
			_team = (OrbitGameManager.teamAssignment)1;
		}
		ChangeColor();
	}

	void ChangeColor()
	{
		int colId = (int)_team - 1;
		Color col =  _gameManager._teamLooks[colId]._mainColor;
		_renderer.sharedMaterial.color = col;
		_renderer.sharedMaterial.SetColor("_EmissionColor", col);
		if(_feedback != null)
		{
			_feedback.SwitchTeam(col, _gameManager._teamLooks[colId]._ledColor, _gameManager._teamLooks[colId]._miniatureSprite);
		}
	}
	
	public Color  GetColor()
	{
		if(_init)
		{
			return _renderer.sharedMaterial.color;
		}
		else
		{
			return Color.white;
		}
	}
}
