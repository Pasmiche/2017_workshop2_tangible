﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UrneScript : MonoBehaviour {

	public RectTransform _recTransform;
	public Transform _projectorObject;
	public float _scale = 1;
	public float _objectScale = 1;
	// Use this for initialization
	
	// Update is called once per frame
	public void SetScale(float f)
	{
		_scale = f;
	}

	public void SetObjScale(float f)
	{
		_objectScale = f;
	}

	void Update () 
	{
		_recTransform.localScale = new Vector3(_scale, _scale, _scale);
		_projectorObject.localScale = new Vector3(_objectScale, _objectScale, _objectScale);
	}
}
