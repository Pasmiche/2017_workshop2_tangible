﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleScoreOverlay : MonoBehaviour 
{

	public CanvasGroup _canvasGroup;
	public Text _scoreText;
	public Image _scoreCircle;
	public float _duration;
	public AnimationCurve _Curve;
	
	// Use this for initialization
	void Start () 
	{
		_canvasGroup.alpha = 0;
	}


	public void ToggleScore(int score, float delay, Color col)
	{
		_scoreText.text = score.ToString();
		_scoreCircle.color = col;
		StartCoroutine(ToggleScoreCoroutine(delay));
	}

	IEnumerator ToggleScoreCoroutine(float delay)
	{
		yield return new WaitForSeconds(delay);
		float val = 0;
		for(float t = 0; t < 1; t+= Time.deltaTime / _duration)
		{
			val = _Curve.Evaluate(t);
			_canvasGroup.alpha = Mathf.Lerp(0,1,val);
			yield return null;
		}
		val = _Curve.Evaluate(1);
		_canvasGroup.alpha = Mathf.Lerp(0,1,val);
	}	
}
