﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttractorState : MonoBehaviour 
{

	public BoxCollider _bounds;

	[Header ("State")]
	public bool _isMoving;
	public bool _isRotating;
	public bool _isAtRest;
	public bool _isGrounded;
	public bool _inBound;
	public int _zone;
	
	[Header ("Parameters")]
	public float _restTimeTresh;
	public float _groundThresh;
    public float _rotationThresh;
    public float _speedThresh;

	[Header ("speed")]
	public float _transformSpeed;
	public float _angularSpeed;
	
	float _restTimer;
	Vector3 _lastPosition;
	Vector3 _lastAngle;

	void Update()
	{
		_zone = 0;
		if(transform.position.x >=0)
		_zone = 1;

		if(_bounds != null)
		{
			_inBound = _bounds.bounds.Contains(transform.position);
		}

		_isMoving = true;
		_isRotating = true;

		if(_transformSpeed < _speedThresh)
		{
			_isMoving = false;
		}
		if(_angularSpeed < _rotationThresh)
		{
			_isRotating = false;
		}
		RestCheck();
		GroundCheck();
	}

	void RestCheck()
	{
		if(!_isMoving && !_isRotating)
		{
			if(!_isAtRest)
			{
				_restTimer += Time.deltaTime;
				if(_restTimer >= _restTimeTresh)
				{
					_isAtRest = true;
				}
			}
		}
		else
		{
			_isAtRest = false;
			_restTimer = 0;
		}
	}

	void GroundCheck()
	{
		_isGrounded = false;
		if(transform.position.y < _groundThresh)
		{
			_isGrounded = true;
		}
	}

	void LateUpdate()
	{
		_transformSpeed = (transform.position - _lastPosition).magnitude;
		_angularSpeed = (transform.eulerAngles - _lastAngle).magnitude;
		_lastPosition = transform.position;
		_lastAngle = transform.eulerAngles;
	}
	
}
