﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidFeedback : MonoBehaviour {


	public Transform _parentAsteroid;
	public ParticleSystem _dots;
	public SpriteRenderer _teamSpriteRenderer;
	public SpriteRenderer _maskSprite;
	public LightCue_Manager _ledManager;
	public Color _inactiveColor;
	public Color _maskColorActive;
	public Color _maskColorInactive;
	// Use this for initialization
	void Start () 
	{

		Renderer renderer = _dots.GetComponent<Renderer>();
		Material mat = Instantiate(renderer.material);
		mat.name = "newMat";
		renderer.material = mat;
		Deactivate();
	}
	
	// Update is called once per frame

	void Update () 
	{
		transform.position = new Vector3 (_parentAsteroid.position.x, _parentAsteroid.position.y + 1, _parentAsteroid.position.z);
	}

	public void Deactivate()
	{
		StartCoroutine(DeactivateCoroutine());
	}

	IEnumerator DeactivateCoroutine()
	{
		if(_ledManager != null)
		{
			_ledManager.ChangeColor(Color.white, 1);
			_ledManager.LightCue("Inactive",1,1,true);
		}
		var main = _dots.main;
		Color col1 = main.startColor.color;
		Renderer pRender = _dots.GetComponent<Renderer>();
		for(float t = 0; t < 1; t+= Time.deltaTime / 0.25f)
		{
			_teamSpriteRenderer.transform.localScale = Vector3.Lerp(new Vector3(0.5f,0.5f,0.5f), Vector3.zero, t);
			_maskSprite.color = Color.Lerp(_maskColorActive, _maskColorInactive, t);
			yield return null;
		}
		_teamSpriteRenderer.transform.localScale = Vector3.zero;
		_maskSprite.color = _maskColorInactive;
		yield return new WaitForSeconds(0.25f);
		for(float t = 0; t < 1; t+= Time.deltaTime / 0.75f)
		{
			main.startColor = Color.Lerp(col1, _inactiveColor, t);
			pRender.sharedMaterial.SetColor("_TintColor",Color.Lerp(col1, _inactiveColor, t));
			yield return null;
		}

		main.startColor = _inactiveColor;
		pRender.sharedMaterial.SetColor("_TintColor", _inactiveColor);
	}

	public void SwitchTeam(Color col, Color lightColor, Sprite teamSprite)
	{
		StartCoroutine(SwitchTeamCortoutine(col, lightColor, teamSprite));
	}

	IEnumerator SwitchTeamCortoutine(Color col, Color lightColor, Sprite teamSprite)
	{
		if(_ledManager != null)
		{
			_ledManager.ChangeColor(lightColor,1);
			_ledManager.LightCue("Sinewave",2,1,false,0.025f,1);
		}
		var main = _dots.main;
		
		Renderer pRender = _dots.GetComponent<Renderer>();
		
		
		_teamSpriteRenderer.sprite = teamSprite;
		for(float t = 0; t < 1; t+= Time.deltaTime / 0.25f)
		{
			_teamSpriteRenderer.transform.localScale = Vector3.Lerp(Vector3.zero, new Vector3(0.5f,0.5f,0.5f), t);
			main.startColor = Color.Lerp(_inactiveColor,col,t);
			pRender.sharedMaterial.SetColor("_TintColor", Color.Lerp(_inactiveColor,col,t));
			_maskSprite.color = Color.Lerp(_maskColorInactive, _maskColorActive, t);
			yield return null;
		}
		_teamSpriteRenderer.transform.localScale =  new Vector3(0.5f,0.5f,0.5f);
		main.startColor = col;
		pRender.sharedMaterial.SetColor("_TintColor", col);
	}

	public void VictoryBlink(Color col)
	{
		_ledManager.ChangeColor(col, 1);
		_ledManager.LightCue("Blink", 0.25f, 0.25f, false);
	}
}
