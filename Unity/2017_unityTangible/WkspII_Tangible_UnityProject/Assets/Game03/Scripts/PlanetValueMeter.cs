﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetValueMeter : MonoBehaviour {

	public GameObject _pointPrefab;
	public List<GameObject> _extraPointsArray = new List<GameObject>();
	public int _extraPoints;

	Coroutine _movePointCoroutine;

	public void AddExtraPoint()
	{
			_extraPoints += 1;
			CalculatePositions();
	}
	
	void CalculatePositions()
	{
		float[] angles = new float[_extraPoints];
		float angleVal = 360.0f / angles.Length;
		for(int i = 0; i< angles.Length; i++)
		{
			angles[i] = angleVal * i;
		}
		CreateObjects(angles);
	}

	void CreateObjects(float[] angles)
	{
		for(int i = 0; i< angles.Length; i++)
		{
			if(_extraPointsArray.Count <= i)
			{
				_extraPointsArray.Add(Instantiate(_pointPrefab,transform.position,Quaternion.identity,transform));
			}
			//_extraPointsArray[i].transform.localEulerAngles =  new Vector3(0, angles[i], 0);
		}
		if(_movePointCoroutine != null)
		{
			StopCoroutine(_movePointCoroutine);
		}
		_movePointCoroutine = StartCoroutine(MovePoints(angles));
	}

	IEnumerator MovePoints(float[] angles)
	{
		float[] currentAngles = new float[_extraPointsArray.Count];
		for(int i = 0; i< currentAngles.Length; i++)
		{
			currentAngles[i] = _extraPointsArray[i].transform.localEulerAngles.y;
		}

		
		for(float t = 0; t< 1; t+= Time.deltaTime / 0.5f)
		{
			for(int i = 0; i< _extraPointsArray.Count; i++)
			{
				float angle = Mathf.Lerp(currentAngles[i], angles[i], t);
				_extraPointsArray[i].transform.localEulerAngles = new Vector3(0, angle, 0);
			}
			yield return null;

		}
		for(int i = 0; i< _extraPointsArray.Count; i++)
		{
			_extraPointsArray[i].transform.localEulerAngles = new Vector3(0, angles[i], 0);
		}
		_movePointCoroutine = null;
	}

	void Update()
	{
		if(Input.GetKeyDown(KeyCode.X))
		{
			_extraPoints += 1;
			CalculatePositions();
		}
	}
}
