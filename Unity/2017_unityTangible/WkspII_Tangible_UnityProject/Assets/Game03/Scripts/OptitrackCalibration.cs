﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptitrackCalibration : MonoBehaviour {

	[Header("Offset Config")]
	public OptitrackConfigJson _config;
	public OptitrackStreamingClient _streamingClient;
	public UrneScript _urneScript;
	public bool _forceClient;

	[Header ("Ui")]
	public GameObject _uiRoot;
	public Text _precisionXYZText;
	public Text _offsetXText;
	public Text _offsetYText;
	public Text _offsetZText;
	public Text _precisionScaleText;
	public Text _scaleOffsetXText;
	public Text _scaleOffsetYText;
	public Text _scaleOffsetZText;

	public float _offsetPrecision;
	public float _scalePrecision;

	bool _uiOnScreen = true;

	void Start()
	{
		_streamingClient = GetComponent<OptitrackStreamingClient>();
		_uiRoot.SetActive(false);
		_uiOnScreen =  false;
		LoadConfig();
	}

	void Update()
	{
		if(Input.GetKeyDown(KeyCode.O))
		{
			_uiRoot.SetActive(!_uiRoot.activeSelf);
			_uiOnScreen =  _uiRoot.activeSelf;
		}

		if(_uiOnScreen)
		{
			_offsetXText.text = _config._xOffset.ToString("F3");
			_offsetYText.text = _config._yOffset.ToString("F3");
			_offsetZText.text = _config._zOffset.ToString("F3");
			_scaleOffsetXText.text = _config._xScale.ToString("F3");
			_scaleOffsetYText.text = _config._yScale.ToString("F3");
			_scaleOffsetZText.text = _config._zScale.ToString("F3");
			_precisionXYZText.text = _offsetPrecision.ToString("F3");
			_precisionScaleText.text = _scalePrecision.ToString("F3");
		}
	}

	public void SaveConfig()
	{
		if(_urneScript != null)
		{
			_config.UrneImageScale = _urneScript._scale;
			_config.UrneObjScale = _urneScript._objectScale;
		}

		if(OrbitGameManager.Instance._pointsToWin != null)
		{
			OrbitGameManager manager = OrbitGameManager.Instance;
			_config.PointsToWin = manager._pointsToWin;
		}

		string path = Application.streamingAssetsPath + "/optitrackConfig.json";
		string json = JsonUtility.ToJson(_config);
		File.WriteAllText(path,json);
	}

	void LoadConfig()
	{
		if(File.Exists(Application.streamingAssetsPath + "/optitrackConfig.json"))
		{
			string path = Application.streamingAssetsPath + "/optitrackConfig.json";
			string json = File.ReadAllText(path);
			_config = JsonUtility.FromJson<OptitrackConfigJson>(json);
			_streamingClient.LocalAddress = _config.LocalIpAdress;
			_streamingClient.ServerAddress = _config.OptitrackServerAdress;
			_streamingClient.ServerCommandPort = (ushort)_config.OptitrackServerCommandPort;
			_streamingClient.ServerDataPort = (ushort)_config.OptitrackServerDataPort;
			if(!Application.isEditor || _forceClient)
			{
				_streamingClient.enabled = true;
			}

			if(_urneScript != null)
			{
				_urneScript._scale =_config.UrneImageScale;
				_urneScript._objectScale = _config.UrneObjScale;
			}

			if(OrbitGameManager.Instance._pointsToWin != null)
			{
				OrbitGameManager manager = OrbitGameManager.Instance;
				manager._pointsToWin = _config.PointsToWin;
			}

			print("Optitrack config file successfully loaded!");
		}
		else
		{
			print("No optitrack config file found");
		}
	}

	public Vector3 GetOffset()
	{
		return new Vector3(_config._xOffset, _config._yOffset, _config._zOffset);
	}

	public Vector3 GetScale()
	{
		return new Vector3(_config._xScale, _config._yScale, _config._zScale);
	}

	public void XOffsetButton(bool left)
	{
		if(left)
		{
			_config._xOffset += _offsetPrecision;
		}
		else
		{
			_config._xOffset -= _offsetPrecision;
		}
	}

	public void YOffsetButton(bool left)
	{
		if(left)
		{
			_config._yOffset += _offsetPrecision;
		}
		else
		{
			_config._yOffset -= _offsetPrecision;
		}
	}

	public void ZOffsetButton(bool left)
	{
		if(left)
		{
			_config._zOffset += _offsetPrecision;
		}
		else
		{
			_config._zOffset -= _offsetPrecision;
		}
	}

	public void XScaleButton(bool left)
	{
		if(left)
		{
			_config._xScale += _scalePrecision;
		}
		else
		{
			_config._xScale -= _scalePrecision;
		}
	}

	public void YScaleButton(bool left)
	{
		if(left)
		{
			_config._yScale += _scalePrecision;
		}
		else
		{
			_config._yScale -= _scalePrecision;
		}
	}

	public void ZScaleButton(bool left)
	{
		if(left)
		{
			_config._zScale += _scalePrecision;
		}
		else
		{
			_config._zScale -= _scalePrecision;
		}
	}

	public void SetOffsetPrecision(float precision)
	{
		_offsetPrecision = precision;
	}

	public void SetScalePrecision(float precision)
	{
		_scalePrecision = precision;
	}
}

[Serializable]
public class OptitrackConfigJson
{
	public float _xOffset;
    public float _yOffset;
    public float _zOffset;
    public float _xScale;
	public float _yScale;
	public float _zScale;

	// for networking
	public string LocalIpAdress;
	public string OptitrackServerAdress;
	public int OptitrackServerCommandPort;
	public int OptitrackServerDataPort;

	// urne projector
	public float UrneImageScale;
	public float UrneObjScale;

	// for orbit game
	public int PointsToWin;

	// for famlly points
	public int SemiWinnerPoints;
	public int SemiLoserPoints;
	public int FinalWinnerPoints;
	public int FinalLoserPoints;
	
}
