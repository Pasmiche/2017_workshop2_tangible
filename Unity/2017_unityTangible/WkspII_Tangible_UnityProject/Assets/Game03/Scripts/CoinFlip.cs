﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinFlip : MonoBehaviour {

	public Vector3 _startPos;
	public Vector3[] _endPoses;

	public AnimationCurve _apparitionAnim;
	public AnimationCurve _disparitionAnim;
	public AnimationCurve _flipAnim;
	public AnimationCurve _flimAnimScale;

	public ParticleSystem _coinParticles;

	[Header ("Audio")]
	public AudioSource _audioSource;
	public AudioClip[] _sfx;

	Vector3 _endSize;
	Renderer _renderer;
	float _startScale = 2;
	OrbitGameManager _manager;

	// Use this for initialization
	void Start () 
	{
		_manager = OrbitGameManager.Instance;
		_renderer = GetComponent<Renderer>();
		_renderer.enabled = false;
		_endSize = new Vector3(_startScale, _startScale, _startScale);
		transform.localScale = Vector3.zero;
	}
	
	// Update is called once per frame

	public void CustomizeCoin(FamilyLook[] looks)
	{
		_renderer.materials[0].SetTexture("_BumpMap", looks[0]._normalMap);
		_renderer.materials[0].color = looks[0]._mainColor;
		
		_renderer.materials[2].SetTexture("_BumpMap", looks[1]._normalMap);
		_renderer.materials[2].color = looks[1]._mainColor;
	}

	public void FlipCoin(int endPos)
	{
		StartCoroutine(FlipCoinCoroutine(_endPoses[endPos]));
	}

	IEnumerator FlipCoinCoroutine(Vector3 endPos)
	{	
		//yield return new WaitForSeconds(1.5f);
		transform.eulerAngles = _startPos;
		_renderer.enabled = true;
		_audioSource.PlayOneShot(_sfx[1]);
		for(float t = 0; t < 1; t += Time.deltaTime / 0.5f)
		{
			float val = _apparitionAnim.Evaluate(t);
			transform.localScale = _endSize * val;
			yield return null;
		}
		_audioSource.PlayOneShot(_sfx[2]);
		for(float t = 0; t < 1; t += Time.deltaTime / 3)
		{	
			float val = _flipAnim.Evaluate(t);
			float scaleVal = _flimAnimScale.Evaluate(t);
			float scale = _startScale + scaleVal;
			transform.eulerAngles = Vector3.Lerp(_startPos, endPos, val);
			transform.localScale = new Vector3(scale, scale, scale);
			yield return null;
		}
		_audioSource.PlayOneShot(_sfx[0]);
		CameraShake.Instance.AddCameraShake(0.2f);
		// show message for teamstart
		_manager._centerDot.SetActive(true);
		for(int i = 0; i< _manager._orbitParticles.Length; i++)
		{
			_manager._orbitParticles[i].SetActive(true);
		}
		PlayParticles();
		yield return new WaitForSeconds(1);
		_audioSource.PlayOneShot(_sfx[1]);
		for(float t = 0; t < 1; t += Time.deltaTime / 0.5f)
		{
			float val = _disparitionAnim.Evaluate(t);
			transform.localScale = _endSize * val;
			yield return null;
		}
		_renderer.enabled = false;

		_manager.CoinFilpCallback();
	}


	void PlayParticles()
	{	
		FamilyLook  look = OrbitGameManager.Instance._teamLooks[0];
		var main = _coinParticles.main;
		main.startColor = look._mainColor;
		var renderMod = _coinParticles.GetComponent<Renderer>();
		renderMod.material.mainTexture = look._miniature;
		renderMod.material.SetColor("_TintColor", look._mainColor);
		_coinParticles.Emit(500);
	}

	public void PlayParticles(FamilyLook look)
	{
		var main = _coinParticles.main;
		main.startColor = look._mainColor;
		var renderMod = _coinParticles.GetComponent<Renderer>();
		renderMod.material.mainTexture = look._miniature;
		renderMod.material.SetColor("_TintColor", look._mainColor);
		_coinParticles.Emit(500);
	}
	
}
