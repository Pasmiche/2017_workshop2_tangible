﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleDestroyOnFinished : MonoBehaviour {

	ParticleSystem _ps;

	void OnEnable()
	{
		_ps = GetComponent<ParticleSystem>();
	}

	void Update()
	{
		if(!_ps.IsAlive())
		{
			Destroy(gameObject);
		}
	}
}
