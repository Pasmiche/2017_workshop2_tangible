﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArtnetConfigManager : Singleton<ArtnetConfigManager> 
{
	
	public MF_ArtNetSender[] _senders;

	void SaveConfig()
	{
		string path = Application.streamingAssetsPath + "/artnetconfig.json";
		ArtnetConfigJson config = new ArtnetConfigJson();
		config.LocalIp = _senders[0]._localIp;
		config.ConfigSpecs = new ArtnetConfig[_senders.Length];
		for(int i = 0; i< _senders.Length; i++)
		{
			config.ConfigSpecs[i] = new ArtnetConfig();
			config.ConfigSpecs[i].DeviceName = _senders[i]._deviceName;
			config.ConfigSpecs[i].RemoteIp = _senders[i]._remoteEndPoint;
			config.ConfigSpecs[i].SubnetMask = _senders[i]._locaSubnetMask;
			config.ConfigSpecs[i].UniverseToSend = _senders[i]._universeToSend;
		}
		config.SaveJson("artnetConfig.json");
	}

	void Update()
	{
		if(Input.GetKeyDown(KeyCode.G))
		{
			SaveConfig();
		}
	}
}

[Serializable]
public class ArtnetConfigJson
{
	public string LocalIp;
	public ArtnetConfig[] ConfigSpecs;
}

[Serializable]
public class ArtnetConfig
{
	public string DeviceName;
	public string RemoteIp;
	public string SubnetMask;
	public int UniverseToSend;
}
