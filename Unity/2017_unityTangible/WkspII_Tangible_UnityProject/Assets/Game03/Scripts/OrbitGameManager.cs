﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class OrbitGameManager : Singleton<OrbitGameManager>
{
    public enum teamAssignment {neutral, one, two};

    [Header ("Team Management")]
    public bool _getRandomTeam;
    public FamilyManager.FamilyNames[] _teamIds = new FamilyManager.FamilyNames[2];
    public Color[] _teamColors;
    public FamilyLook[] _teamLooks;
    public teamAssignment _teamPlaying;
    public FamilyManager _familyManager;
    public CoinFlip _coinFlipper;
    public int _pointsToWin = 30;
    public Gamekeeper.GameMessage _gameMessage;
    public StatsManager _statsManager;
    public bool _finals;

    [Header ("Objects management")]
    public List<Planet> _planets;
    public Asteroid[] _asteroids;
    public Orbit[] _orbits;
    public GameObject[] _orbitParticles;
    public GameObject _centerDot;
    public AsteroidFeedback[] _asteroidFeedback;
    PlanetGenerator _planetGenerator;


    
    [Header ("Audio")]
    public AudioSource _audioSource;
    public AudioClip[] _collisionSounds;
    public AudioClip _scoreSound;
    public AudioClip _apparitionSound;
    public AudioClip _winSound;
    public AudioClip _gameStartSound;
    public AudioSource _mainSoundtrack;
    public float _mainSoundtrackTargetVol;
    int _collisionIndex = 0;

    [Header ("Collision Tracking (combos)")]
    public int _comboLenght;
    public CameraShake _camShake;

    [Header ("UI elements")]
    public IntroManager _introManager;
    public ShowTeamStart _showTeamStart;
    public int[] _scores = new int[2];
    public ToggleScoreOverlay _scoreOverlay;
    public Text[] _scoreTexts;
    public Text _lastTeamThrowText;
    public CollisionCountUi _collisionCountUi;
    public Text _teamWins;
    public LightCue_Manager _lightCueManager;
    public GameObject _finalIndicator;


    bool _init;

    [Space(20)]
    public AsyncMqtt _mqttManager;
    public OptitrackCalibration _optitrackCalib;


    void Start()
    {
        _statsManager = GetComponent<StatsManager>();
        _mainSoundtrackTargetVol = _mainSoundtrack.volume;
        _mainSoundtrack.volume = 0;
        _teamWins.rectTransform.localScale = Vector3.zero;
        _familyManager = GetComponent<FamilyManager>();
        _audioSource.PlayOneShot(_gameStartSound);                //GameStartMessage();
    }

    public void GameStart(Gamekeeper.GameMessage gameMessage)
    {
        if(gameMessage.name != "session_start")
        {
            return;
        }

        if(gameMessage.data.participants.Length != 4)
        {
            print("NOT ENOUGH PLAYERS");
            return;
        }
       
        if(gameMessage.data.participants[0].participant_family.ToLower() == "humain")
        {
            print("________ HUMAIN DETECTED __________");
            gameMessage.data.participants[0].participant_family = "chaba";
        }

        if(gameMessage.data.participants[1].participant_family.ToLower() == "humain")
        {
            print("________ HUMAIN DETECTED __________");
            gameMessage.data.participants[1].participant_family = "chaba";
        }

        if(gameMessage.data.participants[2].participant_family.ToLower() == "humain")
        {
            print("________ HUMAIN DETECTED __________");
            gameMessage.data.participants[2].participant_family = "kemish";
        }

        if(gameMessage.data.participants[3].participant_family.ToLower() == "humain")
        {
            print("________ HUMAIN DETECTED __________");
            gameMessage.data.participants[3].participant_family = "kemish";
        }
        

        _gameMessage = gameMessage;
        if(gameMessage.data.participants.Length == 4)
        {
             _teamIds[0] = StringToNameEnum(_gameMessage.data.participants[0].participant_family);
             _teamIds[1] = StringToNameEnum(_gameMessage.data.participants[2].participant_family);
        }
        else if(gameMessage.data.participants.Length == 2)
        {
             _teamIds[0] = StringToNameEnum(_gameMessage.data.participants[0].participant_family);
             _teamIds[1] = StringToNameEnum(_gameMessage.data.participants[1].participant_family);
        }
        else
        {
            // participant number not good
            return;
        }
        _getRandomTeam = false;
        Init();
    }

    void GameStart()
    {
        _getRandomTeam = true;
        Init();
    }

    public void Init()
    {
        _introManager.Hide(0.5f);
        if(_getRandomTeam)
        {
            List<int> families = new List<int>{0,1,2,3};
            int rnd = UnityEngine.Random.Range(0, families.Count);
            _teamIds[0] = (FamilyManager.FamilyNames)families[rnd];
            families.RemoveAt(rnd);
            rnd = UnityEngine.Random.Range(0, families.Count);
            _teamIds[1] = (FamilyManager.FamilyNames)families[rnd];
        }

        FamilyLook[] looks = new FamilyLook[] {_familyManager.GetFamilyLookByName(_teamIds[0]), _familyManager.GetFamilyLookByName(_teamIds[1])};
        _coinFlipper.CustomizeCoin(looks);
        SelectFirstTeam();
    }


    void SelectFirstTeam()
    {
        int firstTeam = UnityEngine.Random.RandomRange(0, 100) % 2;
        int secondTeam = 0;
        if(firstTeam == 0)
        {
            secondTeam = 1;
        }
        _teamLooks = new FamilyLook[2];
        _teamLooks[0] = _familyManager.GetFamilyLookByName(_teamIds[firstTeam]);
        _teamLooks[1] = _familyManager.GetFamilyLookByName(_teamIds[secondTeam]);
        SendStartToScoreboard();
        _statsManager.Init(_teamLooks[0]._familyName, _teamLooks[1]._familyName);
        _coinFlipper.FlipCoin(firstTeam);
        StartCoroutine(FadeMainTrack());
        _teamPlaying = teamAssignment.one;
    }

    void ApplyColorsAndThemes()
    {
        _teamColors[1] = _teamLooks[0]._mainColor;
        _teamColors[2] = _teamLooks[1]._mainColor;
        _scoreTexts[0].color = _teamLooks[0]._mainColor;
        _scoreTexts[1].color = _teamLooks[1]._mainColor;
        _lightCueManager.LightCue("Sinewave",1,1,false,0.05f,0);
        _lightCueManager.ChangeColor(_teamLooks[0]._ledColor,1);
    }

    void SendStartToScoreboard()
    {
        if(ScoreboardManager.Instance == null)
        {
            return;
        }

        ScoreboardGameState.ScoreboardTeam team1 = new ScoreboardGameState.ScoreboardTeam();
        team1.teamName = _teamLooks[0]._familyName;
        team1.teamColor = _teamLooks[0]._mainColor;
        team1.score = 0;
        team1.teamPlayer = new ScoreboardGameState.ScoreboardPlayer[2];
        team1.teamPlayer[0] = new ScoreboardGameState.ScoreboardPlayer(_gameMessage.PlayerName(0), _gameMessage.PlayerId(0));
        team1.teamPlayer[1] = new ScoreboardGameState.ScoreboardPlayer(_gameMessage.PlayerName(1), _gameMessage.PlayerId(1));

        ScoreboardGameState.ScoreboardTeam team2 = new ScoreboardGameState.ScoreboardTeam();
        team2.teamName = _teamLooks[1]._familyName;
        team2.teamColor = _teamLooks[1]._mainColor;
        team2.score = 0;
        team2.teamPlayer = new ScoreboardGameState.ScoreboardPlayer[2];
        team2.teamPlayer[0] = new ScoreboardGameState.ScoreboardPlayer(_gameMessage.PlayerName(2), _gameMessage.PlayerId(2));
        team2.teamPlayer[1] = new ScoreboardGameState.ScoreboardPlayer(_gameMessage.PlayerName(3), _gameMessage.PlayerId(3));

        ScoreboardGameState.ScoreboardTeam[] teamArray = new ScoreboardGameState.ScoreboardTeam[2] {team1, team2};
        
        ScoreboardManager.Instance.SendGamePresentation(teamArray, 0, 40);
    }

    IEnumerator FadeMainTrack()
    {
        _mainSoundtrack.Play();
        for(float t= 0; t< 1; t+= Time.deltaTime)
        {
            _mainSoundtrack.volume = Mathf.Lerp(0, _mainSoundtrackTargetVol, t);
            yield return null;
        }
        _mainSoundtrack.volume = _mainSoundtrackTargetVol;
    }

    public void CoinFilpCallback()
    {
        //Apply color to pucks and do cool stuff, start game
        print("Callback!!!!");
        foreach(Asteroid a in _asteroids)
        {
            a.Init(teamAssignment.one);
        }
        ApplyColorsAndThemes();

        _planetGenerator = GetComponent<PlanetGenerator>();
        _planetGenerator.Initialize(this);
        _audioSource.PlayOneShot(_apparitionSound);
        _init = true;
        ScoreboardManager.Instance.SendGameStart();
        string topMessage = "les " +_teamLooks[0]._familyName +" commencent!";
        ScoreboardManager.Instance.SendGameMessage(topMessage, "", _teamLooks[0]._mainColor);
        _showTeamStart.ShowFirstTeam(_teamLooks[0]._familyName, _teamLooks[0]._mainColor, 0.5f);
    }

    public void Score(teamAssignment team, int score, float overlayDelay, bool centerpoint)
    {
        _statsManager.UpdatePointScored((int)_teamPlaying -1, centerpoint, score);
        if(score == 0)
            return;
        
        _camShake.AddCameraShake(0.4f);
        PlayScoreSound();
        _scores[(int)team - 1] += score;
        _scoreOverlay.ToggleScore(score, overlayDelay, _teamLooks[(int)team -1]._mainColor);

        string topMessage = score.ToString() + " points,";
        string bottomMessage = "Les " +_teamLooks[(int)team -1]._familyName.ToUpper() + " marquent!";
        ScoreboardManager.Instance.SendGameMessage(topMessage, bottomMessage, _teamLooks[(int)team -1]._mainColor);

        SendScoreToScoreboard();
        if(_scores[(int)team -1] >= _optitrackCalib._config.PointsToWin)
        {
            StartCoroutine(GameEnd(_teamLooks[(int)team -1], (int)team -1));
        }
    }

    void SendScoreToScoreboard()
    {
        if(ScoreboardManager.Instance == null)
        {
            return;
        }
        
        ScoreboardManager.Instance.SendScore(_scores);
    }

    IEnumerator GameEnd(FamilyLook teamLook, int teamId)
    {
        yield return new WaitForSeconds(1);
        for(int i = 0; i < _planets.Count; i++)
        {
            _planets[i].EndgameEffect();
            yield return new WaitForSeconds(0.05f);
        }

        _lightCueManager.ChangeColor(teamLook._ledColor, 0.5f);
        _lightCueManager.LightCue("Blink",0.5f,1,false,0,1);
        _asteroidFeedback[0].VictoryBlink(teamLook._ledColor);
        _asteroidFeedback[1].VictoryBlink(teamLook._ledColor);

        _teamWins.text = teamLook._familyName.ToUpper()  + "\n" + "l'emportent!";
        _teamWins.color = teamLook._mainColor;

        ScoreboardManager.Instance.SendGameEnd(teamId);

        for(float t = 0; t<1; t+= Time.deltaTime / 0.25f)
        {
            _teamWins.rectTransform.localScale = Vector3.Lerp(Vector3.zero,Vector3.one, t);
            yield return null;
        }
        
        _coinFlipper.PlayParticles(teamLook);
        CameraShake.Instance.AddCameraShake(0.3f);
        _audioSource.PlayOneShot(_winSound);
        yield return new WaitForSeconds(3);
        StopOrbitParticle();

         for(float t = 0; t<1; t+= Time.deltaTime / 0.25f)
        {
            _teamWins.rectTransform.localScale = Vector3.Lerp(Vector3.one,Vector3.zero, t);
            yield return null;
        }
        _teamWins.rectTransform.localScale = Vector3.zero;

        SendWinnerMessage(teamLook);
    
        yield return new WaitForSeconds(3f);
        _introManager.Endgame(0.5f);
        float baseVolume = _mainSoundtrack.volume;
        for(float t = 0; t< 1; t += Time.deltaTime)
        {
            _mainSoundtrack.volume = Mathf.Lerp(baseVolume, 0,t);
            yield return null;
        }
        _mainSoundtrack.volume = 0;
        Restart();
    }

    public void SendWinnerMessage(FamilyLook teamLook)
    {
        if(_getRandomTeam)
        {
            return;
        }
        int winnerPoints = 0;
        int loserPoints = 0;
        if(_finals)
        {
            winnerPoints = _optitrackCalib._config.FinalWinnerPoints;
            loserPoints = _optitrackCalib._config.FinalLoserPoints;
        }
        else
        {
            winnerPoints = _optitrackCalib._config.SemiWinnerPoints;
            loserPoints = _optitrackCalib._config.SemiLoserPoints;
        }

        if(_gameMessage.data.participants.Length == 4)
        {
            if(_gameMessage.data.participants[0].participant_family == teamLook._familyName.ToLower())
            {
                _gameMessage.data.participants[0].participant_score = winnerPoints;
                _gameMessage.data.participants[1].participant_score = winnerPoints;
                _gameMessage.data.participants[2].participant_score = loserPoints;
                _gameMessage.data.participants[3].participant_score = loserPoints;
            }
            if(_gameMessage.data.participants[0].participant_family != teamLook._familyName.ToLower())
            {
                _gameMessage.data.participants[0].participant_score = loserPoints;
                _gameMessage.data.participants[1].participant_score = loserPoints;
                _gameMessage.data.participants[2].participant_score = winnerPoints;
                _gameMessage.data.participants[3].participant_score = winnerPoints;
            }
        }
        _gameMessage.type = "action";
        _gameMessage.action = "confirm_session_end";
        string json = JsonUtility.ToJson(_gameMessage);
        json = json.Replace("_namespace", "namespace");
        print ("GAME END MESSAGE: " + json);
        _mqttManager.sendMqttMsgToTopic("connectedfactory/nowhere/gamekeeper/incoming", json);
    }

    public void RemoveAndDestroyPlanet(Planet p)
    {
        _planets.Remove(p);
        Destroy(p.transform.parent.gameObject);
    }

    public void SetTeamThrow(teamAssignment _team)
    {
        if(!_init)
        {
            return;
        }
        _teamPlaying = _team;
        _lastTeamThrowText.text = _teamLooks[(int)_team - 1]._familyName;
    }

	void Update ()
    {
        _scoreTexts[0].text = _scores[0].ToString();
        _scoreTexts[1].text = _scores[1].ToString();

        // manual ctrls //
        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            _asteroids[0].RemoteSwitchTeam();
        }

        if(Input.GetKeyDown(KeyCode.Alpha2))
        {
            _asteroids[1].RemoteSwitchTeam();
        }
        
        if(Input.GetKeyDown(KeyCode.R))
        {
            ScoreboardManager.Instance.ResetGame();
            Restart();
        }

        if(Input.GetKeyDown(KeyCode.I))
        {
            print("Initialize game");
            GameStart();
        }

        if(Input.GetKeyDown(KeyCode.E))
        {
            print("End game");
            Score(teamAssignment.one,30 ,0.7f, true);
        }

        if(Input.GetKeyDown(KeyCode.F))
        {
            print("Toggle Finals");
            _finals = !_finals;
            _finalIndicator.SetActive(_finals);
        }
        
    }

    public void TrackCollisionCount()
    {
        _comboLenght += 1;
        _collisionCountUi.UpdateCollisionCount(_comboLenght);
    }

    public void ResetCollisionCount()
    {
        _comboLenght = 0;
    }

    public void PlayCollisionSound()
    {
        int soundIndex = _collisionIndex % _collisionSounds.Length;
        _audioSource.PlayOneShot(_collisionSounds[soundIndex]);
        _collisionIndex += 1;
    }

    public void PlayScoreSound()
    {
        _audioSource.PlayOneShot(_scoreSound);
    }

    void Restart()
    {
        SceneManager.LoadScene(0);
    }

    void ChangeOrbitParticleColors(FamilyLook look)
    {
        for(int i = 0; i< _orbitParticles.Length; i++)
        {
            ParticleSystem ps = _orbitParticles[i].GetComponent<ParticleSystem>();
            ps.GetComponent<ParticleSystemRenderer>().material.SetColor("_TintColor",look._mainColor);
            ps.GetComponent<ParticleSystemRenderer>().trailMaterial.SetColor ("_TintColor", look._mainColor);
            var main = ps.main;
            main.startColor = look._mainColor;
        }
    }

    void StopOrbitParticle()
    {
        for(int i = 0; i< _orbitParticles.Length; i++)
        {
            ParticleSystem ps = _orbitParticles[i].GetComponent<ParticleSystem>();
            ps.Stop();
        }
    }

    FamilyManager.FamilyNames StringToNameEnum(string name)
    {
        return (FamilyManager.FamilyNames)Enum.Parse(typeof(FamilyManager.FamilyNames), name);
    }
}