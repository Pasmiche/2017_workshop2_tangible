﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : Collidable 
{
    //public OrbitGameManager.teamAssignment _team;
    public string _orbit;
	public float _movementSpeed;
	public LayerMask _layerMask;
	public AnimationCurve _movementCurve;
	public AnimationCurve _movementCurveLanding;
    public GameObject _collisionParticlePrefab;
	public GameObject _scoreParticlePrefab;
	public GameObject _destroySFX;
	public PlanetValueMeter _extraPointMeter;
	public TrailRenderer _trail;
	public float _rotationMult;
	public Orbit _activeOrbit;
	public Collider _collider;

	[Header ("Apparition")]
	public AnimationCurve _apparitionCurve;
	
	[Header ("Combos")]
	public Combo _combos;

	[Header ("Root params")]
	public Transform _root;
	public float _rotationSpeed;

	public bool _isMoving;
	public bool _dangerPlanet;

	public OrbitGameManager _gameManager;
	Renderer _renderer;
	float _textureRotationSpeed;
	int _collisionCount;

	bool _ready;

	void Start()
	{
		if(_dangerPlanet)
		{
			Init(OrbitGameManager.teamAssignment.neutral, "Orbit4", OrbitGameManager.Instance._orbits[3], null);
		}	
	}

	public void Init(OrbitGameManager.teamAssignment team, string orbit, Orbit activeOrbit, GameObject _pointValuePrefab)
	{
		
		_gameManager = OrbitGameManager.Instance;
		if(_extraPointMeter != null)
		{
			_extraPointMeter._pointPrefab = _pointValuePrefab;
		}
		_renderer = GetComponent<Renderer>();
		Material mat = Instantiate(_renderer.material);
		mat.name = "clone";
		_renderer.material = mat;

		_root = transform.parent;
		_layerMask = LayerMask.GetMask(new string[]{"Orbit1", "Orbit2", "Orbit3", "Orbit4"});
		_textureRotationSpeed = Random.Range(0.05f, 0.3f);
		if(Random.value > 0.5f)
		{
			_textureRotationSpeed = -_textureRotationSpeed;
		}

		_team = team;
		_orbit = orbit;
		_activeOrbit = activeOrbit;
		SetColor();
		StartCoroutine(Apparition());
	}

	public void PlanetReady()
	{
		_ready = true;
		StartCoroutine(EngageRotation());
	}

	void Update()
	{
		_renderer.sharedMaterial.mainTextureOffset = new Vector2(0, Time.time * _textureRotationSpeed);
		_renderer.sharedMaterial.SetTextureOffset("_EmissionMap", new Vector2(0, Time.time * _textureRotationSpeed));

		if(!_ready)
		{
			return;
		}

		if(!_isMoving && _activeOrbit != null)
		{
			_rotationSpeed = _activeOrbit._rotationSpeed * _rotationMult;
		}
		_root.transform.Rotate(Vector3.up, _rotationSpeed);
		
	}

	void OnTriggerEnter(Collider other)
	{
		
		if(_isMoving)
		{
			return;
		}

		if(other.name.Contains("Asteroid") || other.name.Contains("Planet"))
		{
			_trail.enabled = true;
			if(other.name.Contains("Asteroid"))
			{
				_gameManager.ResetCollisionCount();
			}

			if(other.name.Contains("Planet"))
			{
				_gameManager.TrackCollisionCount();
				//ManageCombo(other);
				if(!_dangerPlanet)
				{
					AddExtraPoint();
				}
			}

			CameraShake.Instance.AddCameraShake(0.1f);
            Vector3 otherPos = new Vector3(other.transform.position.x,0,other.transform.position.z);
			CollisionFeedback(otherPos);
			_isMoving = true;
			Ray ray = new Ray(transform.position, (otherPos - transform.position) * -1);
			Debug.DrawRay(ray.origin, ray.direction * 1000, Color.red, 0.5f);
			RaycastHit hit;
        	if(Physics.Raycast(ray, out hit, Mathf.Infinity, _layerMask))
            {
                _orbit = hit.transform.root.name;
				_activeOrbit = hit.transform.root.GetComponent<Orbit>();
				Debug.DrawLine(ray.origin, hit.point,  Color.green, 0.5f);
				StartCoroutine(MoveToNewPos(hit.transform.position));
			}
			else
			{
                StartCoroutine(FallBackOnOuterOrbit(transform.position + (ray.direction * 3)));
			}
		}
	}

	IEnumerator MoveToNewPos(Vector3 newPos)
	{	
		float dist = Vector3.Distance(transform.position, newPos);
		float speed = _movementSpeed * dist;
		Vector3 origin = transform.position;
		for(float t = 0; t< 1; t+= Time.deltaTime / speed)
		{
			transform.position = Vector3.Lerp(origin,newPos,_movementCurve.Evaluate(t));
			yield return null;
		}
		transform.position = newPos;
		_isMoving = false;
		if(_activeOrbit.name == "Orbit4" && _team != OrbitGameManager.teamAssignment.neutral)
		{
			StartCoroutine(ScorePoint());
		}
		if(_activeOrbit.name == "Orbit1" && _team ==  OrbitGameManager.teamAssignment.neutral)
		{
			print("Oops!");
			StartCoroutine(Catastrophe());
		}
	}

	IEnumerator FallBackOnOuterOrbit(Vector3 newPos)
	{
		float dist = Vector3.Distance(transform.position, newPos);
		float speed = _movementSpeed * dist;
		Vector3 origin = transform.position;
		for(float t = 0; t< 1; t+= Time.deltaTime / speed / 2)
		{
			transform.position = Vector3.Lerp(origin,newPos,_movementCurve.Evaluate(t));
			yield return null;
		}
		transform.position = newPos;

		Vector3 landingPos = GetLandingPos();
		dist = Vector3.Distance(transform.position, landingPos);
		speed = _movementSpeed * dist;
		origin = transform.position;
		for(float t = 0; t< 1; t+= Time.deltaTime / speed / 2)
		{
			transform.position = Vector3.Lerp(origin, landingPos, _movementCurveLanding.Evaluate(t));
			yield return null;
		}
		transform.position = landingPos;
		_isMoving = false;
	}

	Vector3 GetLandingPos()
	{
		Ray ray = new Ray(transform.position, (Vector3.zero - transform.position));
		Debug.DrawRay(ray.origin, ray.direction * 1000, Color.yellow, 0.5f);
		RaycastHit hit;
		if(Physics.Raycast(ray, out hit, Mathf.Infinity, _layerMask))
		{
			_orbit = hit.transform.root.name;
			_activeOrbit = hit.transform.root.GetComponent<Orbit>();
			Debug.DrawLine(ray.origin, hit.point,  Color.blue, 0.5f);
			return hit.transform.position;
		}
		else
		{
			return transform.position;
		}
	}

	public void AddExtraPoint()
	{
		_extraPointMeter.AddExtraPoint();
	}

	/*void ManageCombo(Collider other)
	{
		Planet p = other.GetComponent<Planet>();
		if(p._combos == null)
		{
			_combos = new Combo();
		}
		else
		{
			_combos = p._combos;
			p._combos =  null;
		}
	}*/

	IEnumerator ScorePoint()
	{	
		ScoreFeedback();
		_gameManager.Score(_team, _extraPointMeter._extraPoints +1, 0.7f, true);
		GetComponent<Collider>().enabled = false;
		for(float t = 0; t< 1; t += Time.deltaTime * 4)
		{
			float size = 1-t;
			transform.localScale = new Vector3(size, size, size);
			yield return null;
		}
		_gameManager.RemoveAndDestroyPlanet(this);
	}

	public void EndgameEffect()
	{
		Instantiate(_collisionParticlePrefab, transform.position,Quaternion.identity);
		//Instantiate(_destroySFX, transform.position, Quaternion.identity);
		Destroy(transform.parent.gameObject);
	}

	IEnumerator Catastrophe()
	{
		print("Shit Coroutine Start");
		for(int i = 0; i< OrbitGameManager.Instance._planets.Count; i++)
		{
			if(OrbitGameManager.Instance._planets[i]._team == OrbitGameManager.Instance._teamPlaying)
			{
				yield return new WaitForSeconds(0.25f);
				OrbitGameManager.Instance._planets[i].DestroyPlanet();
			}
		}
	}

	IEnumerator Apparition()
	{
		_trail.enabled = true;
		transform.localPosition = Vector3.zero;
		transform.localScale = Vector3.zero;
		for(float t= 0; t< 1; t += Time.deltaTime / 0.5f)
		{
			float val = _apparitionCurve.Evaluate(t) * _activeOrbit._radius;
			transform.localPosition = new Vector3(0, 0, val);
			float scaleVal = _apparitionCurve.Evaluate(t);
			transform.localScale = new Vector3(scaleVal, scaleVal, scaleVal);
			yield return null;
		}
		transform.localPosition = new Vector3(0,0,_activeOrbit._radius);
		_collider.enabled = true;
		if(_dangerPlanet)
		{
			PlanetReady();
		}
		/*transform.localScale = Vector3.zero;

		for(float t= 0; t< 1; t += Time.deltaTime / 0.5f)
		{
			float val = _apparitionCurve.Evaluate(t);
			transform.localScale = new Vector3(val, val, val);
			yield return null;
		}
		transform.localScale = Vector3.one;
		if(_dangerPlanet)
		{
			PlanetReady();
		}*/
	}

	IEnumerator EngageRotation()
	{
		for(float t = 0 ; t< 1; t+= Time.deltaTime / 0.25f)
		{
			_rotationMult = t * t;
			yield return null;
		}
		_rotationMult = 1;
	}

	public void DestroyPlanet()
	{
		Instantiate(_collisionParticlePrefab,transform.position,Quaternion.identity);
		Destroy(gameObject);
	}

	void CollisionFeedback(Vector3 otherPos)
	{
		_gameManager.PlayCollisionSound();
		Instantiate(_collisionParticlePrefab, otherPos, Quaternion.identity);
	}

	void ScoreFeedback()
	{
		GameObject go = Instantiate(_scoreParticlePrefab, transform.position, Quaternion.identity);
		ParticleSystem ps = go.GetComponent<ParticleSystem>();
		ps.GetComponent<ParticleSystemRenderer>().material.SetColor("_TintColor", _gameManager._teamColors[(int)_team]);
		ps.GetComponent<ParticleSystemRenderer>().trailMaterial.SetColor ("_TintColor", _gameManager._teamColors[(int)_team]);
		var main = ps.main;
		main.startColor = _gameManager._teamColors[(int)_team];
	}

	void SetColor()
	{
		int colId = (int)_team - 1;
		_renderer.sharedMaterial.color = _gameManager._teamLooks[colId]._mainColor;
		_renderer.sharedMaterial.SetColor("_EmissionColor", _gameManager._teamLooks[colId]._mainColor);
		_trail.startColor =  _gameManager._teamLooks[colId]._mainColor;;
	}
}
