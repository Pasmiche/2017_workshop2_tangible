﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollisionCountUi : MonoBehaviour {

	public Text _countText;
	public RectTransform _transform;
	public ParticleSystem _smokeEffect;

	public bool _isOnScreen;
	public float _hideCountDelay;
	int _colCount;
	float _hideCountCounter;

	public CollisionCountUi _extraPointUi;

	Coroutine _colCountCoroutine;
	OrbitGameManager _gameManager;

	void Start()
	{
		_gameManager = OrbitGameManager.Instance;
		_transform.localScale = Vector3.zero;
		if(_extraPointUi != null)
		{
			_colCount = 10000;
		}
	}

	public void UpdateCollisionCount(int colCount)
	{
		if(colCount == _colCount)
		{
			return;
		}
		_colCount = colCount;

		if(_extraPointUi != null && CollisionsToPoints() > 0)
		{
			_extraPointUi.UpdateCollisionCount(CollisionsToPoints());
		}

		if(!_isOnScreen)
		{
			_colCountCoroutine = StartCoroutine(ShowCount());
		}
		else
		{
			if(_colCountCoroutine == null)
			{
				_colCountCoroutine = StartCoroutine(UpdateCount());
			}
			else
			{
				StopCoroutine(_colCountCoroutine);
				_colCountCoroutine = StartCoroutine(UpdateCount());
			}
		}
	}

	void Update()
	{
		if(_isOnScreen && _colCountCoroutine == null)
		{
			_hideCountCounter += Time.deltaTime;
			if(_hideCountCounter >= _hideCountDelay)
			{
				_colCountCoroutine =  StartCoroutine(HideCount());
			}
		}
	}

	public IEnumerator ShowCount()
	{
		_hideCountCounter = 0;
		_isOnScreen = true;
		_countText.text = "0";
		for(float t = 0; t< 1; t+= Time.deltaTime * 10)
		{
			_transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one * 2, t);
			yield return null;
		}
		_transform.localScale = Vector3.one * 2;
		_countText.text = _colCount.ToString();
		for(float t = 0; t< 1; t+= Time.deltaTime * 20)
		{
			_transform.localScale = Vector3.Lerp(Vector3.one * 2, Vector3.one, t);
			yield return null;
		}
		_transform.localScale = Vector3.one;
		_colCountCoroutine = null;
	}

	IEnumerator UpdateCount()
	{
		_hideCountCounter = 0;
		Vector3 baseScale = _transform.localScale;
	
		_transform.localScale = Vector3.one * 2;
		yield return null;
		_countText.text = _colCount.ToString();
		for(float t = 0; t< 1; t+= Time.deltaTime * 20)
		{
			_transform.localScale = Vector3.Lerp(Vector3.one * 2, Vector3.one, t);
			yield return null;
		}
		_transform.localScale = Vector3.one;
		_colCountCoroutine = null;
	}

	IEnumerator HideCount()
	{
		int points = CollisionsToPoints();
		int teamNumber = (int)_gameManager._teamPlaying -1;
		StatsManager.Instance.SetLongestStrike(teamNumber, _colCount);

		if(points > 0)
		{
			Color col  = _gameManager._teamLooks[teamNumber]._mainColor;
			_gameManager.Score(_gameManager._teamPlaying, points, 0, false);
			var main = _smokeEffect.main;
			main.startColor = col * 0.8f;
			_smokeEffect.GetComponent<Renderer>().material.SetColor("_TintColor", col * 0.8f);
			_smokeEffect.Play();
		}
		for(float t = 0; t< 1; t+= Time.deltaTime * 10)
		{
			_transform.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, t);
			yield return null;
		}
		_transform.localScale = Vector3.zero;
		_isOnScreen = false;
		if(_extraPointUi != null)
		{
			_colCount = 10000;
		}
		_colCountCoroutine = null;
	}

	int CollisionsToPoints()
	{
		if(_colCount < 10)
        {
            return 0;
        }

        int tens  = (_colCount - (_colCount % 10)) / 10;
        return tens;
	}
}
