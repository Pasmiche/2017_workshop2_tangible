﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsManager : Singleton<StatsManager>
{
	public StatsObject[] _teamStats;
	bool _init;
	// Use this for initialization

	public void Init(string teamNameOne, string teamNameTwo)
	{
		print("------------------------ STATS MANAGER INIT CRISS!!! ------------------------- ");
		_teamStats = new StatsObject[2];
		_teamStats[0] = new StatsObject(teamNameOne);
		_teamStats[1] = new StatsObject(teamNameTwo);
	}

	public void UpdateThrowCount(int team)	
	{
		_teamStats[team]._throwCount += 1;
	}

	public void UpadteReboundCount(int team)
	{
		_teamStats[team]._reboundCount += 1;
		
	}

	public void SetLongestStrike(int team, int count)
	{
		if(count > _teamStats[team]._longestStrike)
		{
			_teamStats[team]._longestStrike = count;
		}

		// send to scoreboard manager
		string[] teamNames = new string[] {_teamStats[0]._teamName, _teamStats[1]._teamName};
		string[] statsVal = new string[] {_teamStats[0]._longestStrike.ToString(), _teamStats[1]._longestStrike.ToString()};
		ScoreboardManager.Instance.UpdateGameStats("Record de rebonds:", teamNames, statsVal);
	}

	public void UpdatePointScored(int team, bool centerPoint, int count)
	{
		/*if(_teamStats[team]._centerPointScored == null)
		{
			_teamStats[team]._centerPointScored = new
		}
		if(_teamStats[team]._reboundPointScored == null)
		{

		}*/

		//print("Update Point The FUCK!! " + centerPoint);
		if(centerPoint)
		{
			_teamStats[team]._centerPointScored.Add(count);
		}
		else
		{
			_teamStats[team]._reboundPointScored.Add(count);
		}

		int[] maxCenter = new int[] {0, 0};
		for(int i = 0; i< maxCenter.Length; i++)
		{
			if(_teamStats[i]._centerPointScored != null & _teamStats[i]._centerPointScored.Count > 0)
			{
				maxCenter[i] = _teamStats[i]._centerPointScored.Max();
			}
		}
		
		string[] teamNames = new string[] {_teamStats[0]._teamName, _teamStats[1]._teamName};
		string[] statsVal = new string[] {maxCenter[0].ToString(), maxCenter[1].ToString()};
		ScoreboardManager.Instance.UpdateGameStats("Record de point marqué au centre:", teamNames, statsVal);

		int[] maxRebound = new int[] {0,0};
		for(int i = 0; i< maxRebound.Length; i++)
		{
			if(_teamStats[i]._reboundPointScored != null & _teamStats[i]._reboundPointScored.Count > 0)
			{
				maxRebound[i] = _teamStats[i]._reboundPointScored.Max();
			}
		}

		string[] statsVal2 = new string[] {maxRebound[0].ToString(), maxRebound[1].ToString()};
		ScoreboardManager.Instance.UpdateGameStats("Record de points de rebond marqués", teamNames, statsVal2);
	}
}

[Serializable]
public class StatsObject
{
	public string _teamName;
	public int _throwCount;
	public int _reboundCount;
	public int _longestStrike;
	public List<int> _reboundPointScored;
	public  List<int> _centerPointScored;

	public StatsObject(string name)
	{
		_teamName = name;
		_reboundPointScored = new List<int>();
		_centerPointScored = new List<int>();
	}
}
