﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : Singleton<CameraShake> 
{
	public float _intensity;
	public float _maxIntensity;
	public float _intensityAdd;
	public AnimationCurve _camshakeDecay;
	Vector3 _basePos;
	
	// Use this for initialization
	void Start () 
	{
		_basePos = transform.position;
	}
	
	// Update is called once per frame
	void Update () 
	{
		_intensity = Mathf.Clamp(_intensity,0, _maxIntensity);
		float xNoise = Random.Range (-_intensity, _intensity);
		float yNoise = Random.Range (-_intensity, _intensity);
		float zNoise = Random.Range (-_intensity, _intensity);
		Vector3 camNoise = new Vector3 (xNoise, yNoise, zNoise);
		transform.position = _basePos + camNoise;

		if(_intensity > 0)
		{
			float f = _intensity.Normalize(0, _maxIntensity);
			_intensity -= Time.deltaTime * _camshakeDecay.Evaluate(f);
		}
	}

	public void AddCameraShake(float intensity)
	{
		_intensity += intensity;
	}
}
