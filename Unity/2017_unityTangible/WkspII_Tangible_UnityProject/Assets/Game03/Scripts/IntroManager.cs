﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IntroManager : MonoBehaviour {


	public CanvasGroup _canvasGroup;
	public Image _logo;
	public AnimationCurve _showCurve;
	public AnimationCurve _hideCurve;
	public float _showDuration;
	
	// Use this for initialization
	void Start () 
	{
		_logo.transform.localScale = Vector3.zero;
		_canvasGroup.alpha = 1;	
		StartCoroutine(FadeInIntro());
	}

	IEnumerator FadeInIntro()
	{
		for(float t = 0; t<1; t += Time.deltaTime / _showDuration)
		{
			float f = _showCurve.Evaluate(t);
			_logo.transform.localScale = new Vector3(f,f,f);
			yield return null;
		}
		_logo.transform.localScale = Vector3.one;
	}
	
	public void Hide(float duration)
	{
		StartCoroutine(HideCoroutine(duration));
	}

	IEnumerator HideCoroutine(float duration)
	{
		for(float t = 0; t<1; t += Time.deltaTime / duration)
		{
			float f = _hideCurve.Evaluate(t);
			_logo.transform.localScale = new Vector3(f,f,f);
			_canvasGroup.alpha = 1-t;
			yield return null;
		}
		_canvasGroup.alpha = 0;
		_logo.transform.localScale = Vector3.zero;
	}

	public void Endgame(float duration)
	{
		_logo.transform.localScale = Vector3.zero;
		StartCoroutine(EndgameCoroutine(duration));
	}

	IEnumerator EndgameCoroutine(float duration)
	{
		for(float t = 0; t<1; t += Time.deltaTime / duration)
		{
			_canvasGroup.alpha = t;
			yield return null;
		}
		_canvasGroup.alpha = 1;
	}
}
