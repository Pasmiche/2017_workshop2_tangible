﻿using UnityEngine;
using System.Collections;

public class ClickResult : MonoBehaviour {

	public virtual void Click(int inputId)
	{
		Debug.Log ("I have been clicked!" + gameObject.transform.position + ",  mouseInputID = " + inputId);
	}
}
