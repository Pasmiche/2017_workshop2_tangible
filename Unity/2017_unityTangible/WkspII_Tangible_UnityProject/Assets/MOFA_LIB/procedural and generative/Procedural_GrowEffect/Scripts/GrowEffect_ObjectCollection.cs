﻿using UnityEngine;
using System.Collections;

public class GrowEffect_ObjectCollection : MonoBehaviour {

	GameObject[] _collection;
	float _growTime;
	GameObject _dummy;

	public void SetCollection(GameObject[] collection, GameObject dummy,  float growTime)
	{
		_collection = collection;
		_growTime = growTime;
		_dummy = dummy;
		foreach (GameObject obj in _collection)
		{
			obj.GetComponent<GrowEffect_GrowObject>().Grow (_growTime);
		}
	}

	public void DestroyObjects()
	{
		Destroy (transform.GetChild (0).gameObject);
		foreach(GameObject obj in _collection)
		{
			obj.GetComponent<GrowEffect_GrowObject>().HideObjects(1.0f);
		}
		Invoke("DestroyDummy",1);
	}

	void DestroyDummy()
	{
		Destroy (_dummy);
		Destroy (gameObject);
	}


}
