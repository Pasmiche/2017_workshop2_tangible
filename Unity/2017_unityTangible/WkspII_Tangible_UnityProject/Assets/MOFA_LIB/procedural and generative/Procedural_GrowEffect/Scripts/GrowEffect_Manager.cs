﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class GrowEffect_Manager : MonoBehaviour {

	public float _growTime = 1;
	public float _growthWidth = 1;
	public float _safetyZone =0.2f;
	public float _largeObjectNbr = 3; 		//per unit
	public float _medObjectNbr = 10;  		//per unit
	public float _smallObjectNbr = 10000;		//per unit

	public float _smallRadius = 0.1f;
	public float _medRadius = 0.5f;
	public float _largeRadius = 1.0f;
		
	public GameObject _devicePrefab;
	public GameObject[] _smallObjects;
	public GameObject[] _mediumObjects;
	public GameObject[] _largeObjects;

	public enum _objType {small,med,large};
	
	GameObject _dummy;
	GameObject _device;
	List<GrowEffect_Growable> _objList = new List<GrowEffect_Growable> ();
	List<GameObject> _gameObjList = new List<GameObject> ();
	List<Vector4> _growZones = new List<Vector4> ();
	
	void Update()
	{
		if(Input.GetKeyDown(KeyCode.G))
		{
			GrowSomething();
		}
	}

	public void GrowSomething()
	{
		GrowSomething(transform.position,transform.localScale * 0, 0);
	}

	public void GrowSomething(Vector3 zonePos, Vector3 zoneSize, float zoneAngle)
	{
		_dummy = new GameObject ();
		_dummy.name = "dummy";
		_dummy.transform.position = zonePos;
		_device = Instantiate (_devicePrefab, zonePos, Quaternion.Euler(new Vector3 (0,zoneAngle,0))) as GameObject;
		_device.transform.localScale = zoneSize;
		_objList.Clear ();
		_growZones.Clear ();
		_gameObjList.Clear ();
		SetGrowZones (zonePos,zoneSize);
		//Debug.Log ("Grow Zone Count " + _growZones.Count);
		foreach(Vector4 zone in _growZones)
		{
			int zonePointsL = GetDistribution(zone,_largeObjectNbr);
			for (int i=0; i<zonePointsL; i++)
			{
				SetGrowPoints (zone, zonePos.y, _objType.large);
			}
		}
		foreach(Vector4 zone in _growZones)
		{

			int zonePointsM = GetDistribution(zone,_medObjectNbr);
			for (int i=0; i<zonePointsM; i++)
			{
				SetGrowPoints (zone, zonePos.y,_objType.med);
			}
		}
		foreach(Vector4 zone in _growZones)
		{
			int zonePointsS = GetDistribution(zone,_smallObjectNbr);
			for (int i=0; i<zonePointsS; i++)
			{
				SetGrowPoints (zone, zonePos.y, _objType.small);
			}
		}
		_dummy.transform.rotation = Quaternion.Euler (new Vector3 (0, zoneAngle, 0));
		GrowObjects ();
	}

	void SetGrowZones(Vector3 zonePos, Vector3 zoneSize)
	{
		float growthWidth = _growthWidth + _safetyZone;
		Vector2 zone1XBounds = new Vector2(zonePos.x -zoneSize.x/2 - growthWidth, (zonePos.x - zoneSize.x/2)-_safetyZone);
		Vector2 zone1ZBounds = new Vector2((zonePos.z -zoneSize.z/2) - growthWidth, (zonePos.z + zoneSize.z/2) + growthWidth);
		Vector4 zone0 = new Vector4 (zone1XBounds.x, zone1XBounds.y, zone1ZBounds.x, zone1ZBounds.y);

		Vector2 zone2XBounds = new Vector2(zonePos.x -zoneSize.x/2 - _safetyZone, (zonePos.x + zoneSize.x/2) +_safetyZone);
		Vector2 zone2ZBounds = new Vector2((zonePos.z + zoneSize.z/2)+ _safetyZone, (zonePos.z + zoneSize.z/2) + growthWidth);
		Vector4 zone1 = new Vector4 (zone2XBounds.x, zone2XBounds.y, zone2ZBounds.x, zone2ZBounds.y);

		Vector2 zone3XBounds = new Vector2((zonePos.x + zoneSize.x/2) + _safetyZone, (zonePos.x + zoneSize.x/2) + growthWidth);
		Vector2 zone3ZBounds = new Vector2((zonePos.z -zoneSize.z/2) - growthWidth, (zonePos.z + zoneSize.z/2) + growthWidth);
		Vector4 zone2 = new Vector4 (zone3XBounds.x, zone3XBounds.y, zone3ZBounds.x, zone3ZBounds.y);

		Vector2 zone4XBounds = new Vector2((zonePos.x -zoneSize.x/2) - _safetyZone, (zonePos.x + zoneSize.x/2) + _safetyZone);
		Vector2 zone4ZBounds = new Vector2((zonePos.z - zoneSize.z/2) - growthWidth, (zonePos.z - zoneSize.z/2)- _safetyZone);
		Vector4 zone3 = new Vector4 (zone4XBounds.x, zone4XBounds.y, zone4ZBounds.x, zone4ZBounds.y);

		_growZones.Add (zone0);
		_growZones.Add (zone1);
		_growZones.Add (zone2);
		_growZones.Add (zone3);

	}
	

	public void SetGrowPoints(Vector4 zone, float posY, _objType type)
	{
		Vector3 newPos = new Vector3(UnityEngine.Random.Range(zone.x,zone.y), posY, UnityEngine.Random.Range(zone.z,zone.w));
		GrowEffect_Growable newObj = new GrowEffect_Growable(type,newPos,ReturnSize(type));
		CreatePointList (newObj);
	}

//	public void SetGrowPointCircle()
//	{
//		Vector2 randomPos = UnityEngine.Random.insideUnitCircle;
//		Vector3 newPos = new Vector3 (randomPos.x, 0, randomPos.x);
//	}

	void CreatePointList(GrowEffect_Growable newObj)
	{
		if(_objList.Count == 0)
		{
			_objList.Add(newObj);
			CreateObject(newObj);
		}
		else
		{
			bool goodPos = true;
			foreach(GrowEffect_Growable obj in _objList)
			{
				if(Vector3.Distance(obj._position,newObj._position) < obj._size)
				{
					goodPos = false;
				}
			}
			if(goodPos)
			{
				_objList.Add(newObj);
				CreateObject(newObj);
			}
		}
	}

	void CreateObject(GrowEffect_Growable obj)
	{	
		float rndRotate = UnityEngine.Random.Range (-180.0f, 180.0f);
		GameObject newObj = Instantiate (ReturnPrefab(obj._type), obj._position, Quaternion.Euler(new Vector3(0,rndRotate,0))) as GameObject;
		newObj.transform.localScale = Vector3.zero;
		newObj.transform.parent = _dummy.transform;
		_gameObjList.Add (newObj);
	}

	void GrowObjects()
	{
		_device.GetComponent<GrowEffect_ObjectCollection> ().SetCollection (_gameObjList.ToArray(), _dummy, _growTime);
	}

	float ReturnSize(_objType type)
	{
		if(type == _objType.large)
		{
			return _largeRadius;
		}
		else if(type == _objType.med)
		{
			return _medRadius;
		}
		else if(type == _objType.small)
		{
			return _smallRadius;
		}
		else
		{
			return 1.0f;
		}
	}
	GameObject ReturnPrefab(_objType type)
	{
		if(type == _objType.large)
		{
			if(_largeObjects == null || _largeObjects.Length == 0)
			{
				return null;
			}
			int rnd = UnityEngine.Random.Range(0,_largeObjects.Length);
			return _largeObjects[rnd];
		}
		if(type == _objType.med)
		{
			if(_mediumObjects == null || _mediumObjects.Length == 0)
			{
				return null;
			}
			int rnd = UnityEngine.Random.Range(0,_mediumObjects.Length);
			return _mediumObjects[rnd];
		}
		if(type == _objType.small)
		{
			if(_smallObjects == null || _smallObjects.Length == 0)
			{
				return null;
			}
			int rnd = UnityEngine.Random.Range(0,_smallObjects.Length);
			return _smallObjects[rnd];
		}
		else
		{
			return null;
		}
	}

	int GetDistribution (Vector4 zone, float number)
	{
		float zoneArea = (zone.y - zone.x) * (zone.w - zone.z);
		int distribution = Convert.ToInt32(number * zoneArea);
		return distribution;
	}
}
