﻿using UnityEngine;
using System.Collections;

public class GrowEffect_GrowObject : MonoBehaviour {

	public bool _isGrass = false;
	float _duration = 0;

	public void Grow(float duration)
	{
		_duration = duration;
		StartCoroutine ("GrowCoroutine");
	}

	IEnumerator GrowCoroutine()
	{
		float sizef = Random.Range (0.6f, 1f);
		Vector3 size = new Vector3 (sizef, sizef, sizef);
		for(float t = 0; t < 1; t+= Time.deltaTime / _duration)
		{
			transform.localScale = Vector3.Lerp (Vector3.zero, size, t*t*t * (t * (6f*t - 15f) + 10f));
			yield return null;
		}
		
	}

	public void HideObjects(float duration)
	{
		StopCoroutine ("GrowCoroutine");
		StartCoroutine("HideCoroutine");
	}

	IEnumerator HideCoroutine()
	{
		Vector3 size = transform.localScale;
		for(float t = 0; t < 1; t+= Time.deltaTime*2)
		{
			transform.localScale = Vector3.Lerp (size,Vector3.zero, t*t*t);
			yield return null;
		}

		DestroyMe ();
	}

	void DestroyMe()
	{
		Destroy (gameObject);
	}
}
