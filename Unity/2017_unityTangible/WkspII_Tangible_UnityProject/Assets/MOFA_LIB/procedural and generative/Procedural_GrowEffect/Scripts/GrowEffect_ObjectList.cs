﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GrowEffect_ObjectList : MonoBehaviour {

	GameObject[] _objectList;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetObjectList(List<GameObject> _objects)
	{
		_objectList = _objects.ToArray ();
	}
}
