﻿using UnityEngine;
using System.Collections;

public class ClickBehaviour_Raycast : MonoBehaviour {

	public enum ClickButton{lmb, rmb, mmb}
	public ClickButton inputUsed;
	public LayerMask _layerMask;


	// Update is called once per frame
	void Update () {

		if (Input.GetMouseButtonDown ((int)inputUsed))
		{
			CastRay();
		}
	}

	void CastRay()
	{
		{
			Ray myRay =  Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if(Physics.Raycast (myRay, out hit,Mathf.Infinity,_layerMask))
			{
				Click (hit,(int)inputUsed);
			}
		}
	}

	void Click(RaycastHit hit, int mouseInput)
	{
		hit.transform.gameObject.GetComponent<ClickResult>().Click(mouseInput);
	}


}
