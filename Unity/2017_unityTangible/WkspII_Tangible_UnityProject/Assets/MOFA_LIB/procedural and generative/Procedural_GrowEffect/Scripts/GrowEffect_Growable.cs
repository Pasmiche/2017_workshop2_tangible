﻿using UnityEngine;
using System.Collections;

public class GrowEffect_Growable {

	public GrowEffect_Manager._objType _type;
	public Vector3 _position;
	public float _size;

	public GrowEffect_Growable()
	{

	}

	public GrowEffect_Growable(GrowEffect_Manager._objType type, Vector3 pos, float size)
	{
		_type = type;
		_position = pos;
		_size = size;
	}


}
