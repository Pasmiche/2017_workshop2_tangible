﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Net;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using ArtNet.Sockets;
using ArtNet.Packets;

public class MF_ArtnetReciever : MonoBehaviour 
{
	[Header ("Artnet Config")]
	public string _localIp = "10.10.80.39";
	public string _locaSubnetMask = "255.255.255.0";
	public int _UniverseLowRange = 0;		//min 0
	public int _UniverseHighRange = 255;  	//max 255
	public bool _testSend = false;
	[Header ("Debug")]
	public Texture2D _debugTexture;
	public Image _debugImage;

	ArtNetSocket _socket;
	bool _artnetActive = false;

	Byte[][] _byteJaggedArray;

	// Use this for initialization
	void InitArtnet () 
	{
		_socket = new ArtNetSocket ();
		_socket.EnableBroadcast = false;
		_socket.Open(IPAddress.Parse(_localIp),IPAddress.Parse(_locaSubnetMask));
		_socket.NewPacket += artnet_NewPacket;
		int range = (_UniverseHighRange - _UniverseLowRange) +1;
		_byteJaggedArray = new byte[range][];
		for( int i = 0; i< _byteJaggedArray.Length;i++)
		{
			_byteJaggedArray [i] = new byte[512];
		}
		CreateTestTexture (range);
		_artnetActive = true;
	}

	void CloseArtnet()
	{
		_artnetActive = false;
		_socket.NewPacket -= artnet_NewPacket;
		_socket.Close ();
	}

	public void Toggle(bool toggleOn)
	{
		if (toggleOn)
		{
			if (!_artnetActive)
			{
				InitArtnet ();
			}
		}
		else
		{
			if (_artnetActive)
			{
				CloseArtnet ();
			}
		}
	}

	public void Restart()
	{
		_artnetActive = false;
		if (_socket != null)
		{
			_socket.NewPacket -= artnet_NewPacket;
			_socket.Close ();
		}

		InitArtnet ();
	}

	void OnDisable()
	{
		_artnetActive = false;
		_socket.NewPacket -= artnet_NewPacket;
		_socket.Close ();
	}

	// looks for Dmx packets that are in the desired universe range and store them
	void artnet_NewPacket(object sender, NewPacketEventArgs<ArtNetPacket> e)
	{
		if (e.Packet.OpCode == ArtNet.Enums.ArtNetOpCodes.Dmx)
		{
			lock (_byteJaggedArray)
			{
				ArtNetDmxPacket packet = e.Packet as ArtNetDmxPacket;
				int u = packet.Universe;
				if (u >= _UniverseLowRange && u<= _UniverseHighRange )
				{
					_byteJaggedArray [u - _UniverseLowRange] = packet.DmxData;
				}
			}
		}
	}
		
	void CreateTestTexture(int width)
	{
		_debugTexture = new Texture2D(Mathf.FloorToInt((170)),_byteJaggedArray.Length,TextureFormat.ARGB32,false);
		_debugTexture.filterMode = FilterMode.Point;
		//_debugImage.material.mainTexture = _debugTexture;
		Sprite mySprite = Sprite.Create (_debugTexture, new Rect (0, 0, 170, width), new Vector2 (0, 1));
		_debugImage.sprite = mySprite;
		_debugImage.GetComponent<RectTransform> ().sizeDelta = new Vector2 (170 * 2, width * 2);
	}

	void Update()
	{
//		if (Input.GetKeyDown (KeyCode.A))
//		{
//			InitArtnet ();
//		}
		if (_artnetActive)
		{
			ParsePacketList ();
		}

		if (_testSend)
		{
			byte[] values = new byte[512];
			for (int i = 0; i < 512; i++)
			{
				if (i % 2 == 0)
				{
					values[i] = Convert.ToByte(UnityEngine.Random.Range(127,255));
				}
				else
				{
					values[i] = Convert.ToByte(UnityEngine.Random.Range(0, 127));
				}
			}
			SendArtnet(0,values);
		}
	}

	// do what you want with the packets here
	void ParsePacketList()
	{
		Color32[] pixels = new Color32[_byteJaggedArray.Length * 170];
		lock(_byteJaggedArray)
		{
			try
			{
				int pixelIndex = 0;
				for (int i = 0; i < _byteJaggedArray.Length; i++)
				{
					for (int j = 0; j < _byteJaggedArray [i].Length - 2; j += 3)
					{
						pixels [pixelIndex] = new Color32 (_byteJaggedArray [i] [j], _byteJaggedArray [i] [j + 1], _byteJaggedArray [i] [j + 2],255);
						pixelIndex += 1;
					}
				}
			}
			catch(Exception e)
			{
				Debug.Log(e);
			}
		}
		UpdateTexture (pixels);
	}

	void SendArtnet(int universe, byte[] data)
	{
		ArtNetDmxPacket toSend = new ArtNetDmxPacket ();
		toSend.DmxData = data;
		toSend.Universe = (short)universe;
		//_socket.Send (toSend);

	}

	void UpdateTexture(Color32[] pixels)
	{
		_debugTexture.SetPixels32 (pixels);
		_debugTexture.Apply ();
	}

}
