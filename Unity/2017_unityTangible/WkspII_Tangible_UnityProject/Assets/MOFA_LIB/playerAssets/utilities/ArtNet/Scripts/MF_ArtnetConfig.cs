﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Net;
using System.Xml;
using System.Collections;

public class MF_ArtnetConfig : MonoBehaviour 
{
	public MF_ArtnetReciever _reciever;
	public GameObject _configCanvas;

	[Header("UI Stuff")]
	public Text _logText;
	public Text _ipPlaceholder;
	public Text _maskPlaceholder;
	public Text _lowPlaceholder;
	public Text _highPlaceholder;
	public Toggle _toggle;

	string _filePath = "";
	bool _modKeyPressed = false;



	void Start()
	{
		_filePath = Application.streamingAssetsPath + "/ArtNetConfig.xml";
		LoadConfig ();
	}

	public void Init()
	{
		_logText.text = "LOG: ";
		_ipPlaceholder.text = _reciever._localIp.ToString ();
		_maskPlaceholder.text = _reciever._locaSubnetMask.ToString ();
		_lowPlaceholder.text = _reciever._UniverseLowRange.ToString ();
		_highPlaceholder.text = _reciever._UniverseHighRange.ToString ();
	}

	void Update()
	{
		
		if (Input.GetKeyDown (KeyCode.A))
		{
			Debug.Log ("A key pressed: toggle Artnet Config GUI");
			_configCanvas.SetActive (!_configCanvas.activeSelf);
		}
	}

	public void SetIp(string ip)
	{
		IPAddress newAdress;
		if (IPAddress.TryParse (ip, out newAdress))
		{
			_reciever._localIp = ip;
		} 
		else
		{
			ShowLog (2, "Wrong Ip Adress format");
		}
	}

	public void SetLocalSubnetMask(string mask)
	{
		IPAddress newAdress;
		if (IPAddress.TryParse (mask, out newAdress))
		{
			_reciever._locaSubnetMask = mask;
		} 
		else
		{
			ShowLog (2, "Wrong Ip mask format");
		}
	}

	public void SetLowRange(string low)
	{
		_reciever._UniverseLowRange = Convert.ToInt32(low);
	}

	public void SetHighRange(string high)
	{
		_reciever._UniverseHighRange = Convert.ToInt32(high);
	}

	public void Toggle(bool toggleOn)
	{
		if (_reciever._UniverseHighRange < _reciever._UniverseLowRange)
		{
			ShowLog (2,"Range incorrect!");
		}

		_reciever.Toggle (toggleOn);
	}
		

	public void ShowLog(int level, string message)
	{
		if (level == 0)
		{
			_logText.color = Color.green;
		}
		if (level == 1)
		{
			_logText.color = Color.yellow;
		}
		if (level == 2)
		{
			_logText.color = Color.red;
		}
		CancelInvoke ();
		_logText.text = "LOG: " + message;
		Invoke ("HideLog", 5);
	}

	void HideLog()
	{
		_logText.text = "LOG: ";
	}

	void LoadConfig()
	{
		if (File.Exists (_filePath))
		{
			XmlDocument xmlDoc = new XmlDocument ();
			xmlDoc.Load (_filePath);
			XmlNodeList nodeList = xmlDoc.SelectNodes ("/Config");
			foreach (XmlNode xmlNode in nodeList)
			{
				_reciever._localIp = xmlNode.SelectSingleNode ("ip").InnerText;
				_reciever._locaSubnetMask = xmlNode.SelectSingleNode ("subnetmask").InnerText;
				_reciever._UniverseLowRange = Convert.ToInt32(xmlNode.SelectSingleNode ("lowrange").InnerText);
				_reciever._UniverseHighRange = Convert.ToInt32(xmlNode.SelectSingleNode ("highrange").InnerText);
			}
			Init ();
			Debug.Log("ArtNet Xml Loaded!");
			ShowLog (0,"ArtNet Xml Loaded!");
			_reciever.Restart();
			_toggle.isOn = true;

		}
		else
		{
			ShowLog(1,"No Config Xml Found!");
		}
	}

	public void SaveConfig()
	{
		if (File.Exists (_filePath))
		{
			XmlDocument xmlDoc = new XmlDocument ();
			xmlDoc.Load (_filePath);
			XmlNode newIp = xmlDoc.SelectSingleNode ("Config/ip");
			newIp.InnerText = _reciever._localIp;

			XmlNode newSubnetMask = xmlDoc.SelectSingleNode ("Config/subnetmask");
			newSubnetMask.InnerText = _reciever._locaSubnetMask;

			XmlNode newLowRange = xmlDoc.SelectSingleNode ("Config/lowrange");
			newLowRange.InnerText = _reciever._UniverseLowRange.ToString ();

			XmlNode newHighRange = xmlDoc.SelectSingleNode ("Config/highrange");
			newHighRange.InnerText = _reciever._UniverseHighRange.ToString ();

			xmlDoc.Save (_filePath);
			ShowLog (0, "Config XML Saved Successfully!");
			Debug.Log ("OSC Xml Saved!");
		}
		else
		{
			ShowLog (1, "No ArtNetConfig.xml Found!");
			Debug.Log("NoXmlFound!");
		}
	}


}
