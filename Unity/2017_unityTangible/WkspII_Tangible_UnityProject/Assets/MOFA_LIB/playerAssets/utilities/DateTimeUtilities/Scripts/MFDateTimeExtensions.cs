﻿using System;

public static class MFDateTimeExtensions
{
    public static DateTime EpochToDateTime(this long timestamp)
    {
        DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        dateTime = dateTime.AddSeconds(timestamp);
        return dateTime;
    }

    public static DateTime EpochToDateTimeLocal(this long timestamp)
    {
        DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        dateTime = dateTime.AddSeconds(timestamp);
        return dateTime.ToLocalTime();
    }

    public static double DateTimeToEpoch(this DateTime time)
    {
        DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        double timeDiff = time.Subtract(start).TotalSeconds;
        double epoch = Convert.ToDouble(timeDiff);
        return (epoch);
    }

    public static double DateTimeToEpochLocal(this DateTime time)
    {
        DateTime start = new DateTime(1970, 1, 1, 0, 0, 0);
        time = time.ToUniversalTime();
        double timeDiff = time.Subtract(start).TotalSeconds;
        double epoch = Convert.ToDouble(timeDiff);
        return (epoch);
    }
}
