﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SendLightCuesExample))]
public class SendLightCuesExample_Inspector : Editor 
{

	public override void OnInspectorGUI()
	{

		DrawDefaultInspector();

		SendLightCuesExample myTarget = (SendLightCuesExample)target;

		if(GUILayout.Button("Full On"))
		{
			myTarget.SendCueOne();
		}

		if(GUILayout.Button("Synced SineWave"))
		{
			myTarget.SendCueTwo();
		}

		if(GUILayout.Button("Saw With Offset"))
		{
			myTarget.SendCueThree();
		}

		if(GUILayout.Button("Resync Lights over 2 sec"))
		{
			myTarget.SendCueFour();
		}

		if(GUILayout.Button("Black"))
		{
			myTarget.SendCueFive();
		}

		if(GUILayout.Button("Blink"))
		{
			myTarget.SendCueSix();
		}

		if(GUILayout.Button("Change Color"))
		{
			myTarget.ChangeColor();
		}
	}
}
