﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightCue_LightTrack {


	public LightCue_Behavior _behavior;
	public float _intensity;
	
	public float _offsetMod; //0 to 1, used to lerp between full offset and zero offset
	public float _targetOffset;
	public float _offsetTransitionDuration;

	float _playTime;
	float _playSpeed = 1;
	float _intensityFloor;

	public bool _offsetOn;

	public bool _play;
	public bool _playOnce;

	public LightCue_LightTrack (LightCue_Behavior behavior, float speed, bool playOnce, Vector2 targetOffset, float offsetTransitionDuration, float intensityFloor)
	{
		_intensityFloor = intensityFloor;
		_behavior = behavior;
		_playOnce = playOnce;
		SetSpeedPlayspeed(speed);
		_targetOffset = Random.Range(targetOffset.x, targetOffset.y);
		_offsetTransitionDuration = offsetTransitionDuration;
		if(_targetOffset > 0)
		{
			_offsetOn = true;
		}
		_play = true;
	}

	public LightCue_LightTrack (LightCue_Behavior behavior, float speed, bool playOnce, float targetOffset, float offsetTransitionDuration, float intensityFloor)
	{
		_intensityFloor = intensityFloor;
		_behavior = behavior;
		_playOnce = playOnce;
		SetSpeedPlayspeed(speed);
		_targetOffset = targetOffset;
		_offsetTransitionDuration = offsetTransitionDuration;
		if(_targetOffset > 0)
		{
			_offsetOn = true;
		}
		_play = true;
	}

	void SetSpeedPlayspeed(float speed)
	{
		if(speed != 0)
		{
			_playSpeed = 1 / speed;
		}
		else
		{
			_playSpeed = 1;
		}
	}

	public void ReSync()
	{
		_offsetOn = false;
	}

	public void ReSync(float offsetTransitionDuration)
	{
		_offsetTransitionDuration = offsetTransitionDuration;
		_offsetOn = false;

	}

	public void RemoteUpdate()
	{
		if(_play)
		{
			if(_offsetOn && _offsetMod < 1)
			{
				_offsetMod += Time.deltaTime / _offsetTransitionDuration;
				if(_offsetMod > 1)
				{
					_offsetMod = 1;
				}
			}

			if(!_offsetOn && _offsetMod > 0)
			{
				_offsetMod -= Time.deltaTime / _offsetTransitionDuration;
				if(_offsetMod < 0)
				{
					_offsetMod = 0;
				}
			}
			
			float offsetVal = Mathf.Lerp(0, _targetOffset, _offsetMod);
			_playTime += Time.deltaTime * _playSpeed;
			float intensityVal = _behavior._animationCurve.Evaluate(_playTime + offsetVal);
			_intensity = Mathf.Lerp(_intensityFloor, 1, intensityVal);
			//_intensity = _behavior._animationCurve.Evaluate(_playTime + offsetVal);
		}

		if(_playOnce)
		{
			if(_playTime >= 1)
			{
				_play = false;
			}
		}
	}



	
}



