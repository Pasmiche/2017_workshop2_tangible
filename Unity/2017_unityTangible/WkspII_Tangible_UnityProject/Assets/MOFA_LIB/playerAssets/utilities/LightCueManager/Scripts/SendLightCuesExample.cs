﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendLightCuesExample : MonoBehaviour {

	public LightCue_Manager _lightcueManager;
	public Color _color = Color.red;

	public void SendCueOne()
	{
		_lightcueManager.LightCue("White",1,1,false);
	}

	public void SendCueTwo()
	{
		_lightcueManager.LightCue("Sinewave", 0.5f,2,false);
	}

	public void SendCueThree()
	{
		_lightcueManager.LightCue("Saw", 1f,2,false,0.2f,5);
	}

	public void SendCueFour()
	{
		_lightcueManager.ResyncLights(2);
	}

	public void SendCueFive()
	{
		_lightcueManager.LightCue("Black", 1,0.5f,false);
	}

	public void SendCueSix()
	{
		_lightcueManager.LightCue("Blink", 1,5,false,0.1f,5);
	}

	public void ChangeColor()
	{
		_lightcueManager.ChangeColor(_color,1);
	}
}
