// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:Standard,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:33050,y:32717,varname:node_3138,prsc:2|emission-8836-OUT,alpha-4882-OUT,refract-2921-OUT;n:type:ShaderForge.SFN_Color,id:7241,x:31904,y:32615,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_7241,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Fresnel,id:4882,x:32235,y:32926,varname:node_4882,prsc:2|NRM-2977-OUT,EXP-5204-OUT;n:type:ShaderForge.SFN_Slider,id:5204,x:31888,y:33036,ptovrint:False,ptlb:Fresnel,ptin:_Fresnel,varname:node_5204,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:5.700855,max:10;n:type:ShaderForge.SFN_NormalVector,id:2977,x:31863,y:32866,prsc:2,pt:False;n:type:ShaderForge.SFN_Multiply,id:6842,x:32438,y:32813,varname:node_6842,prsc:2|A-7241-RGB,B-4882-OUT;n:type:ShaderForge.SFN_Slider,id:3032,x:32359,y:33016,ptovrint:False,ptlb:Emission,ptin:_Emission,varname:node_3032,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1.068376,max:10;n:type:ShaderForge.SFN_Multiply,id:8836,x:32748,y:32802,varname:node_8836,prsc:2|A-6842-OUT,B-3032-OUT;n:type:ShaderForge.SFN_TexCoord,id:853,x:31841,y:33315,varname:node_853,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Multiply,id:8578,x:32626,y:33246,varname:node_8578,prsc:2|A-595-OUT,B-6694-OUT;n:type:ShaderForge.SFN_Slider,id:3119,x:31777,y:33590,ptovrint:False,ptlb:Displace,ptin:_Displace,varname:node_3119,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.1025641,max:1;n:type:ShaderForge.SFN_Tex2d,id:1218,x:32240,y:33194,ptovrint:False,ptlb:node_1218,ptin:_node_1218,varname:node_1218,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:bbab0a6f7bae9cf42bf057d8ee2755f6,ntxv:3,isnm:True|UVIN-853-UVOUT;n:type:ShaderForge.SFN_ComponentMask,id:595,x:32437,y:33194,varname:node_595,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-1218-RGB;n:type:ShaderForge.SFN_Multiply,id:1700,x:32487,y:33549,varname:node_1700,prsc:2|A-853-UVOUT,B-3119-OUT;n:type:ShaderForge.SFN_Add,id:2921,x:32812,y:33234,varname:node_2921,prsc:2|A-8578-OUT,B-1700-OUT;n:type:ShaderForge.SFN_Slider,id:6694,x:32098,y:33450,ptovrint:False,ptlb:Noise,ptin:_Noise,varname:node_6694,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;proporder:7241-5204-3032-3119-1218-6694;pass:END;sub:END;*/

Shader "Custom/Bubbles" {
    Properties {
        _Color ("Color", Color) = (1,1,1,1)
        _Fresnel ("Fresnel", Range(0, 10)) = 5.700855
        _Emission ("Emission", Range(0, 10)) = 1.068376
        _Displace ("Displace", Range(0, 1)) = 0.1025641
        _node_1218 ("node_1218", 2D) = "bump" {}
        _Noise ("Noise", Range(0, 1)) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        GrabPass{ }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma only_renderers d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _GrabTexture;
            uniform float4 _Color;
            uniform float _Fresnel;
            uniform float _Emission;
            uniform float _Displace;
            uniform sampler2D _node_1218; uniform float4 _node_1218_ST;
            uniform float _Noise;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 projPos : TEXCOORD3;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 _node_1218_var = UnpackNormal(tex2D(_node_1218,TRANSFORM_TEX(i.uv0, _node_1218)));
                float2 node_8578 = (_node_1218_var.rgb.rg*_Noise);
                float2 sceneUVs = (i.projPos.xy / i.projPos.w) + (node_8578+(i.uv0*_Displace));
                float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
////// Lighting:
////// Emissive:
                float node_4882 = pow(1.0-max(0,dot(i.normalDir, viewDirection)),_Fresnel);
                float3 emissive = ((_Color.rgb*node_4882)*_Emission);
                float3 finalColor = emissive;
                return fixed4(lerp(sceneColor.rgb, finalColor,node_4882),1);
            }
            ENDCG
        }
    }
    FallBack "Standard"
}
