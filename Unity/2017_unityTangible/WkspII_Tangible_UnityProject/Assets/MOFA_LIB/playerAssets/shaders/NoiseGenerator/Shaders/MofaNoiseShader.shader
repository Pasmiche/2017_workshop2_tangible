﻿Shader "Custom/MofaNoise"
{
	Properties
	{
		// Does it vary with time.
		[Toggle(USE_TIME)] _UseTime("UseTime", Float) = 0
		// If it does vary with time, at which speed.
		_Speed("Speed", Float) = 1

		// Noise type
		[Header(Noise)]
		[KeywordEnum(Perlin2D, NormalizedPerlin2D, Simplex)] _NoiseType("NoiseType", Float) = 0
		// Return the absolute value of the noise
		[Toggle] _AbsValue("AbsValue", Float) = 0
		// Return 1 - noise
		[Toggle] _Inverse("Inverse", Float) = 0

		// Scale of the texture
		_Scale("Scale", Float) = 5
		// Shift the UVs (wraps)
		_PhaseShift("PhaseShift", Vector) = (0, 0, 0, 0)
		// Octaves for fractional brownian motion
		_Octaves("Octaves", Int) = 3
		// Return color * Intensity
		_Intensity("Intensity", Float) = 1
		// Return color + offset
		_ValueOffset("ValueOffset", Float) = 0
		// Use for the random permutation, kind of a seed
		_PermutationModulo("PermutationModulo", Range(2, 289)) = 289

	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			Name "Noise"

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag

			#pragma multi_compile _NOISETYPE_PERLIN2D _NOISETYPE_SIMPLEX _NOISETYPE_NORMALIZEDPERLIN2D
			#pragma shader_feature USE_TIME
			#pragma shader_feature _ABSVALUE_ON
			#pragma shader_feature _INVERSE_ON

			#include "MofaNoiseCommon.cginc"

			float _Speed;
			float _ValueOffset;

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = (float4)1;

				#ifdef _NOISETYPE_PERLIN2D
						
					#ifdef USE_TIME
						col = fbmPerlinNoise(frac(i.uv + _PhaseShift + _Time.xx * _Speed) * floor(_Scale), floor(_Scale), _Intensity, _Octaves);
					#else
						col = fbmPerlinNoise(frac(i.uv + _PhaseShift) * floor(_Scale), floor(_Scale), _Intensity, _Octaves);
					#endif

					#ifdef _ABSVALUE_ON
						col = abs(col);
					#endif
				#elif _NOISETYPE_SIMPLEX
					#ifdef USE_TIME
						col = fbmSimplexNoise(i.uv, _Scale, _PhaseShift + _Time.xx * _Speed, _Intensity, _Octaves);
					#else
						col = fbmSimplexNoise(i.uv, _Scale, _PhaseShift, _Intensity, _Octaves);
					#endif
				#elif _NOISETYPE_NORMALIZEDPERLIN2D
					#ifdef USE_TIME
						col = fbmNormalizedPerlinNoise(frac(i.uv + _PhaseShift + _Time.xx * _Speed) * floor(_Scale), floor(_Scale), _Intensity, _Octaves);
					#else
						col = fbmNormalizedPerlinNoise(frac(i.uv + _PhaseShift) * floor(_Scale), floor(_Scale), _Intensity, _Octaves);
					#endif
				#endif

				#ifdef _INVERSE_ON
						col = 1 - col;
				#endif

				col += _ValueOffset;

				return col;
			}

			ENDCG
		}

	}
}
