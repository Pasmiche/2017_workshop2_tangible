#include "UnityCG.cginc"

#define PI 3.14159265359	
// (sqrt(5) - 1)/4 = F4, used once below
#define F4 0.309016994374947451

float _Scale;
float4 _PhaseShift;
float _Octaves;
float _Intensity;
float _PermutationModulo;

// Fade function defined by Ken Perlin
float2 fade(float2 t) {
	return t*t*t*(t*(t*6.0 - 15.0) + 10.0);
}

float4 permute(float4 x)
{
	return fmod((x*34.0 + 1.0)*x, _PermutationModulo);
}

float4 taylorInvSqrt(float4 r)
{
	return (float4)1.79284291400159 - r * 0.85373472095314;
}

float4 grad4(float j, float4 ip) {
	const float4 ones = float4(1.0, 1.0, 1.0, -1.0);
	float4 p, s;

	p.xyz = floor(frac((float3)j * ip.xyz) * 7.0) * ip.z - 1.0;
	p.w = 1.5 - dot(abs(p.xyz), ones.xyz);

	s.x = p.x < 0;
	s.y = p.y < 0;
	s.z = p.z < 0;
	s.w = p.w < 0;
	
	p.xyz = p.xyz + (s.xyz*2.0 - 1.0) * s.www;

	return p;
}

// Perlin Noise ----------------------------------------------

// Perlin noise
// Returns a value between -1 and 1
float periodicPerlinNoise(float2 P, float2 period)
{
	float4 Pi = floor(P.xyxy) + float4(0.0, 0.0, 1.0, 1.0);
	float4 Pf = frac(P.xyxy) - float4(0.0, 0.0, 1.0, 1.0);
	Pi = fmod(Pi, period.xyxy); // To create noise with explicit period
	float4 ix = Pi.xzxz;
	float4 iy = Pi.yyww;
	float4 fx = Pf.xzxz;
	float4 fy = Pf.yyww;

	float4 i = permute(permute(ix) + iy);

	float4 gx = frac(i / 41.0) * 2.0 - 1.0;
	float4 gy = abs(gx) - 0.5;
	float4 tx = floor(gx + 0.5);
	gx = gx - tx;

	float2 g00 = float2(gx.x, gy.x);
	float2 g10 = float2(gx.y, gy.y);
	float2 g01 = float2(gx.z, gy.z);
	float2 g11 = float2(gx.w, gy.w);

	float4 norm = taylorInvSqrt(float4(dot(g00, g00), dot(g01, g01), dot(g10, g10), dot(g11, g11)));
	g00 *= norm.x;
	g01 *= norm.y;
	g10 *= norm.z;
	g11 *= norm.w;

	float n00 = dot(g00, float2(fx.x, fy.x));
	float n10 = dot(g10, float2(fx.y, fy.y));
	float n01 = dot(g01, float2(fx.z, fy.z));
	float n11 = dot(g11, float2(fx.w, fy.w));

	float2 fade_xy = fade(Pf.xy);
	float2 n_x = lerp(float2(n00, n01), float2(n10, n11), fade_xy.x);
	float n_xy = lerp(n_x.x, n_x.y, fade_xy.y);
	return 2.3 * n_xy;
}

// Perlin noise
// Returns a value between 0 and 1
float normalizedPerlinNoise(float2 P, float scale)
{
	return periodicPerlinNoise(P, scale) * 0.5 + 0.5;
}

float fbmNormalizedPerlinNoise(float2 v, float scale, float intensity, int octaves)
{
	float total = 0;
	float frequency = 1;
	float amp = 0.5;

	for (int i = 0; i <= octaves; ++i)
	{
		total += normalizedPerlinNoise(v * frequency, scale) * amp;
		frequency *= 2;
		amp *= 0.5;
	}

	return total * intensity;
}

float fbmPerlinNoise(float2 v, float scale, float intensity, int octaves)
{
	float total = 0;
	float frequency = 1;
	float amp = 0.5;

	for (int i = 0; i <= octaves; ++i)
	{
		total += periodicPerlinNoise(v * frequency, scale) * amp;
		frequency *= 2;
		amp *= 0.5;
	}

	return total * intensity;
}

// Simplex --------------------------------------------------

float simplexNoise(float4 v) 
{
	const float4  C = float4(0.138196601125011,  // (5 - sqrt(5))/20  G4
		0.276393202250021,  // 2 * G4
		0.414589803375032,  // 3 * G4
		-0.447213595499958); // -1 + 4 * G4

							 // First corner
	float4 i = floor(v + dot(v, (float4)F4));
	float4 x0 = v - i + dot(i, C.xxxx);

	// Other corners

	// Rank sorting originally contributed by Bill Licea-Kane, AMD (formerly ATI)
	float4 i0;
	float3 isX = step(x0.yzw, x0.xxx);
	float3 isYZ = step(x0.zww, x0.yyz);
	//  i0.x = dot( isX, vec3( 1.0 ) );
	i0.x = isX.x + isX.y + isX.z;
	i0.yzw = 1.0 - isX;
	//  i0.y += dot( isYZ.xy, vec2( 1.0 ) );
	i0.y += isYZ.x + isYZ.y;
	i0.zw += 1.0 - isYZ.xy;
	i0.z += isYZ.z;
	i0.w += 1.0 - isYZ.z;

	// i0 now contains the unique values 0,1,2,3 in each channel
	float4 i3 = clamp(i0, 0.0, 1.0);
	float4 i2 = clamp(i0 - 1.0, 0.0, 1.0);
	float4 i1 = clamp(i0 - 2.0, 0.0, 1.0);

	//  x0 = x0 - 0.0 + 0.0 * C.xxxx
	//  x1 = x0 - i1  + 1.0 * C.xxxx
	//  x2 = x0 - i2  + 2.0 * C.xxxx
	//  x3 = x0 - i3  + 3.0 * C.xxxx
	//  x4 = x0 - 1.0 + 4.0 * C.xxxx
	float4 x1 = x0 - i1 + C.xxxx;
	float4 x2 = x0 - i2 + C.yyyy;
	float4 x3 = x0 - i3 + C.zzzz;
	float4 x4 = x0 + C.wwww;

	// Permutations
	float j0 = permute(permute(permute(permute(i.w) + i.z) + i.y) + i.x);
	float4 j1 = permute(permute(permute(permute(
		  i.w + float4(i1.w, i2.w, i3.w, 1.0))
		+ i.z + float4(i1.z, i2.z, i3.z, 1.0))
		+ i.y + float4(i1.y, i2.y, i3.y, 1.0))
		+ i.x + float4(i1.x, i2.x, i3.x, 1.0));

	// Gradients: 7x7x6 points over a cube, mapped onto a 4-cross polytope
	// 7*7*6 = 294, which is close to the ring size 17*17 = 289.
	float4 ip = float4(1.0 / 294.0, 1.0 / 49.0, 1.0 / 7.0, 0.0);

	float4 p0 = grad4(j0, ip);
	float4 p1 = grad4(j1.x, ip);
	float4 p2 = grad4(j1.y, ip);
	float4 p3 = grad4(j1.z, ip);
	float4 p4 = grad4(j1.w, ip);

	// Normalise gradients
	float4 norm = taylorInvSqrt(float4(dot(p0, p0), dot(p1, p1), dot(p2, p2), dot(p3, p3)));
	p0 *= norm.x;
	p1 *= norm.y;
	p2 *= norm.z;
	p3 *= norm.w;
	p4 *= taylorInvSqrt(dot(p4, p4));

	// Mix contributions from the five corners
	float3 m0 = max(0.6 - float3(dot(x0, x0), dot(x1, x1), dot(x2, x2)), 0.0);
	float2 m1 = max(0.6 - float2(dot(x3, x3), dot(x4, x4)), 0.0);
	m0 = m0 * m0;
	m1 = m1 * m1;
	return 49.0 * (dot(m0*m0, float3(dot(p0, x0), dot(p1, x1), dot(p2, x2)))
		+ dot(m1*m1, float2(dot(p3, x3), dot(p4, x4))));

}

float surface(float4 coord) 
{
	float n = 0.0;

	n += 0.25 * abs(simplexNoise(coord * 4.0));
	n += 0.5 * abs(simplexNoise(coord * 8.0));
	n += 0.25 * abs(simplexNoise(coord * 16.0));
	n += 0.125 * abs(simplexNoise(coord * 32.0));

	return n;

}

float tileableSimplexNoise(float2 uv, float scale, float2 shift, float amplitude)
{
	// Tiling 4d noise based on
	// https://gamedev.stackexchange.com/questions/23625/how-do-you-generate-tileable-perlin-noise/23639#23639
	float multiplier = scale / (2.0 * PI);
	float nx = cos((uv.x + shift.x) * 2.0 * PI) * multiplier;
	float ny = cos((uv.y + shift.y) * 2.0 * PI) * multiplier;
	float nz = sin((uv.x + shift.x) * 2.0 * PI) * multiplier;
	float nw = sin((uv.y + shift.y) * 2.0 * PI) * multiplier;

	return surface(float4(nx, ny, nz, nw)) * amplitude;
}

float fbmSimplexNoise(float2 v, float scale, float2 shift, float intensity, int octaves)
{
	float total = 0;
	float frequency = 1;
	float amp = 0.5;

	for (int i = 0; i <= octaves; ++i)
	{
		total += tileableSimplexNoise(v * frequency, scale, shift, intensity) * amp;
		frequency *= 2;
		amp *= 0.5;
	}

	return total * intensity;
}

// -----------------------------------------------------------