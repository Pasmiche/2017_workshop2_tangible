using UnityEngine;
using System;
using System.Net;
using System.Diagnostics;
using System.Collections.Generic;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using uPLibrary.Networking.M2Mqtt.Utility;
using uPLibrary.Networking.M2Mqtt.Exceptions;

public enum MQTTlogStateEnum
{
    NoLogs = 0,
    ErrorOnly = 1,
    Everything = 2
}

public class AsyncMqtt : MonoBehaviour
{
    [Header("SERVER")]
    public bool _connected = false;
    public string _address = "connectmqtt.mofa.studio"; 
    public int _port = 8883; 
    public string _subscribeTo;
    string _logTopic;
    public MQTTlogStateEnum _logState = MQTTlogStateEnum.Everything;
    string _clientId;
    Stopwatch _failedConnectionStopwatch;
    MqttClient _client;
    List<string> _messageList = new List<string>();
    System.Security.Cryptography.X509Certificates.X509Certificate _certificate;

    public delegate void MessageDelegate(string newMQTTmsg);
    public MessageDelegate _MQTT_messageListener; // Usage:  GetComponent<AsyncMqtt>()._MQTT_messageListener += AddReceivedMqttMessage; (OnEnable & OnDisable)


    [Header("CERTIFICATE")]
    public bool _useSSL;
    public bool _cert_loadFromStreamingAssets = false;
    public string _cert_loadString = "";
 

    [Header("DEBUG")]
    public bool _useDebug = false;
    public string _subscribeTopic_DEBUG;

    [Header ("GameKeeper")]
    public Gamekeeper.GameMessage _gameMessage;


    void Start()
    {
        if (_useDebug)
            _subscribeTo = _subscribeTopic_DEBUG;

        _logTopic = _subscribeTo + "/logs";
        ConnectToBroker();
    }


    // to use with an xml config file
    /*public void Init (string xmlAddress, int port, MQTTlogStateEnum logLevel, string subscribeTo)
    {
        IPHostEntry host;
        host = Dns.GetHostEntry(xmlAddress);
        for (int i = 0; i < host.AddressList.Length; i++)
        {
            print(host.AddressList[i]);
        }
        //_address = host.AddressList[0].ToString();
        _port = port;
        _subscribeTo = subscribeTo;
        _logTopic = _subscribeTo + "/logs";
        _logState = logLevel;

        ConnectToBroker();
    }*/


    void OnDisable()
    {
        if (_connected)
        {
            Application.logMessageReceived -= LogToMQTT;
            _connected = false;
            _client.Disconnect();
        }
    }

    void ConnectToBroker()
    {
        MQTTJson infos = new MQTTJson();
        infos = infos.LoadJson("mqttconfig.json");
        _address = infos.Hostname;
        _port = infos.Port;
        _useSSL = infos.UseSSL;
        
        try
        {
            UnityEngine.Debug.Log("Try connect to mqtt broker...");
            _clientId = Guid.NewGuid().ToString();
            if(_useSSL)
            {
                // Load new certificate
                byte[] certStr;
                if(_cert_loadString == "")
                {
                    _cert_loadFromStreamingAssets = true;
                }
                if (_cert_loadFromStreamingAssets)
                {
                    _certificate = new System.Security.Cryptography.X509Certificates.X509Certificate();
                    _certificate.Import(Application.streamingAssetsPath + "/mofa_studio_ca_cert.ca");
                }
                else
                {
                    // Load certificate from string
                    certStr = System.Text.Encoding.UTF8.GetBytes(_cert_loadString);
                    _certificate = new System.Security.Cryptography.X509Certificates.X509Certificate(certStr);
                }
                _client = new MqttClient(_address, _port, true, _certificate);
                _client.Connect(_clientId, "connectedfactory", "UhkzYQgubAouyV4uQEULadAA");
            }
            else
            {
                _client = new MqttClient(_address);
                _client.Connect(_clientId);
            }
            
            _client.MqttMsgPublishReceived += client_MqttMsgPublishReceived;
            // subscribe
            _client.Subscribe(new string[] { _subscribeTo }, new byte[] { MqttMsgBase.QOS_LEVEL_AT_MOST_ONCE });
            _connected = true;
            Application.logMessageReceived += LogToMQTT;
            SendMqttMessage("Unity Connected!");
            UnityEngine.Debug.Log("Connected to broker");
        }
        catch (Exception e)
        {
            UnityEngine.Debug.LogError("MQTT ERROR" + e);
        }

        if(_connected)
        {
            GamestartMessage startMessage = new GamestartMessage();
            startMessage.game_id = "ws_tangible";
            startMessage.hostname = "mf-pi-blackbox";
            string json = JsonUtility.ToJson(startMessage);
            //UnityEngine.Debug.Log(json);
            sendMqttMsgToTopic("connectedfactory/nowhere/gamekeeper/setgame", json);
        }
    }


    void Update()
    {
        if (_connected)
        {
            ParseMqttMessages();
            /*try
            {
                
            }
            catch (Exception e)
            {
                UnityEngine.Debug.LogError("MQTT MESSAGE ERROR: " + e);
            }*/
        }
    }


    void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e) 
	{ 
        lock(_messageList)
        {
            _messageList.Add(System.Text.Encoding.UTF8.GetString(e.Message));
        }
    }


    // sends unity log to MQTT broker according to log level
    public void LogToMQTT(string logString, string stackTrace, LogType type)
    {
        string ts = DateTime.Now.ToString();
        switch (_logState)
        {
            // no logs
            case MQTTlogStateEnum.NoLogs:
                break;

            // errors and exceptions
            case MQTTlogStateEnum.ErrorOnly:
                if (type == LogType.Error || type == LogType.Exception)
                {
                    _client.Publish(_logTopic, System.Text.Encoding.UTF8.GetBytes(ts + ": " + logString), MqttMsgBase.QOS_LEVEL_AT_MOST_ONCE,false);
                }
                break;
            
            // everything
            case MQTTlogStateEnum.Everything:
                _client.Publish(_logTopic, System.Text.Encoding.UTF8.GetBytes(ts + ": " + logString), MqttMsgBase.QOS_LEVEL_AT_MOST_ONCE, false);
                break;
        }
    }


    public void SendMqttMessage(string message)
    {
        if (_connected)
        {
            _client.Publish(_logTopic, System.Text.Encoding.UTF8.GetBytes(message), MqttMsgBase.QOS_LEVEL_AT_MOST_ONCE, false);
        }
    }
   

    void ParseMqttMessages()
    {
        lock(_messageList);
        foreach (string message in _messageList)
        {
            print ("MQTT Received: " + message);

            // Register Messages
            //_MQTT_messageListener(message); 

            // Update Log State
            if (message.Contains("log0"))
            {
                _logState = MQTTlogStateEnum.NoLogs;
            }
            if (message.Contains("log1"))
            {
                _logState = MQTTlogStateEnum.ErrorOnly;
            }
            if (message.Contains("log2"))
            {
                _logState = MQTTlogStateEnum.Everything;
            }

            if(message.Contains("ws_tangible"))
            {
                print("GAME MESSAGE RECEIVED!");
                string fixedMessage = message.Replace("namespace", "_namespace");
                _gameMessage = JsonUtility.FromJson<Gamekeeper.GameMessage>(fixedMessage);
                OrbitGameManager.Instance.GameStart(_gameMessage);
            }
        }
        _messageList.Clear();
    }

    public void sendMqttMsgToTopic(string topic, string message)
    {
        UnityEngine.Debug.Log("SENDING MESSAGE: " + topic + " " + message);
        if (_connected)
            _client.Publish(topic, System.Text.Encoding.UTF8.GetBytes(message), MqttMsgBase.QOS_LEVEL_AT_MOST_ONCE, false);
    }
}

[Serializable]
public class GamestartMessage
{
    public string game_id;  //"ws_tangible"
    public string hostname; //"mf-pi-blackbox"
}

[Serializable]
public class MQTTJson
{
	public string Hostname;
	public int Port;
	public bool UseSSL;
}

