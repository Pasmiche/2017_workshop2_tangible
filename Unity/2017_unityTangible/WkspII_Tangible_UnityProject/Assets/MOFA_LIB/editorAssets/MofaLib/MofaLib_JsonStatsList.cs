﻿using System;
using System.Collections.Generic;

[Serializable]
public class MofaLib_JsonStatsList
{
    public List<MofaLib_StatsData> UsageData;
}

[Serializable]
public class MofaLib_StatsData
{
    public string PackageName;
    public string ImportDate;
    public string UnityProjectName;
    public string MofaCodeName;
    public string PCName;
}
