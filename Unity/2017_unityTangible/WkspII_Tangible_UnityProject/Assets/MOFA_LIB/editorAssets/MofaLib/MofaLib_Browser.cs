﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Diagnostics;


namespace MofaLib
{
	public class MofalibPaths : IEquatable<MofalibPaths> , IComparable<MofalibPaths>
	{
		public string path = "";
		public string folderPath = "";
		public string name = "";
		public string category = "";
		public string preview = "";
		public string link = "";
		public string videoLink = "";

		public bool Equals(MofalibPaths other) 
		{
			if (other == null)
			{
				return false;
			}

			if (this.category == other.category)
			{
				return true;
			}
			else 
			{
				return false;
			}
		}
		
		public int CompareTo(MofalibPaths comparePart)
		{
			// A null value means that this object is greater. 
			if (comparePart == null)
			{
				return 1;
			}
			else
			{
				return this.category.CompareTo(comparePart.category);
			}
		}
	}


	public class MofaLib_Browser : EditorWindow 
	{
		List<MofalibPaths> _packagesPaths = new List<MofalibPaths>();

		public string _MofaLibPath = "";
		public string MofaLibConfig = "";

		string _helpFile= "";
		string _googleDriveVideos = "";
		string _searchField = "";
		bool init = false;
		Vector2 scrollPosition = Vector2.zero;


		[MenuItem("MofaLib/MofaLib Package Browser")]

		public static void ShowWindow()
		{
			MofaLib_Browser window = (MofaLib_Browser)EditorWindow.GetWindow(typeof(MofaLib_Browser));
			window.title = "LibBrowser";
			//window.maxSize = new Vector2 (500,1000);
			window.minSize = new Vector2 (530,300);
		}

		void Update()
		{
			if(!init)
			{
				Init();
			}
		}

		void Init()
		{
			string configPath = Application.dataPath + "/mofalibConfig.txt";
			if (File.Exists (configPath)) 
			{
				string text = System.IO.File.ReadAllText(configPath);
				_MofaLibPath = text;
			}

			if (_MofaLibPath != "")
			{
				SearchForPackages();
				GetHelpFileLink();
				GetDriveVideoLocationLink();
			}
			init = true;
		}
		
		void OnGUI()
		{
			scrollPosition = GUILayout.BeginScrollView (scrollPosition,false,true,GUILayout.Width(position.width),GUILayout.Height(position.height));
			if (GUILayout.Button ("MOFALIB PATH: " + _MofaLibPath,GUILayout.Height(30)))
			{
				OpenBrowser();
			}
			if (GUILayout.Button ("Search for packages",GUILayout.Height(25)))
			{
				SearchForPackages();
				GetHelpFileLink();
				GetDriveVideoLocationLink();
			}
			if (_helpFile != "")
			{
				if (GUILayout.Button ("MofaLib Documentation"))
				{
					EditorUtility.OpenWithDefaultApp(_helpFile);
				}
			}
			if (_googleDriveVideos != "")
			{
				if (GUILayout.Button ("Google Drive Video Preview Location"))
				{
					EditorUtility.OpenWithDefaultApp(_googleDriveVideos);
				}
			}

			// search tool
			GUILayout.BeginHorizontal ();
			GUILayout.Label ("Search Keywords: ",GUILayout.Width(150));	
			_searchField = GUILayout.TextField (_searchField, 100, GUILayout.Width(300));
			GUILayout.EndHorizontal ();

			// end serach tool

			if(_packagesPaths.Count != 0)
			{
				string category ="";
				foreach(MofalibPaths path in _packagesPaths)
				{
					if(path.category != category && path.name.ToLower().Contains(_searchField.ToLower()) )
					{
						GUILayout.BeginHorizontal();
						GUILayout.Label ("");	
						GUILayout.EndHorizontal();
						//
						GUILayout.BeginHorizontal();
						GUILayout.Label (path.category,EditorStyles.boldLabel);	
						GUILayout.EndHorizontal();
						category = path.category;
					}
					GUILayout.BeginHorizontal();

					if( path.name.ToLower().Contains(_searchField.ToLower()) || _searchField == "" )
					{
				// folder button
						if (GUILayout.Button ("F",GUILayout.Width(25)))
						{
							try
							{
								Process.Start("explorer.exe",(true ? "/root," : "/select,") + path.folderPath);
							}
							catch ( System.ComponentModel.Win32Exception e )
							{
								// tried to open win explorer in mac
								// just silently skip error
								// we currently have no platform define for the current OS we are in, so we resort to this
								e.HelpLink = ""; // do anything with this variable to silence warning about not using it
							}
						}
				// package button
						if (GUILayout.Button (path.name,GUILayout.Width(275)))
						{
							EditorUtility.OpenWithDefaultApp(path.path);
						}

				// video preview button
						if(path.videoLink != "")
						{
							if (GUILayout.Button ("V",GUILayout.Width(25)))
							{
								EditorUtility.OpenWithDefaultApp(path.videoLink);
							}
						}
						else
						{
							GUILayout.Button (" ",GUILayout.Width(25));
						}

				// image preview button
						if(path.preview != "")
						{
							if (GUILayout.Button ("I",GUILayout.Width(25)))
							{
								MofaLib_PreviewWindow.ShowPreviewWindow();
								MofaLib_PreviewWindow.LoadPreviewImage(path.preview);
							}
						}
						else 
						{
							GUILayout.Button (" ",GUILayout.Width(25));
						}

				// redmine link button
						if(path.link != "")
						{
							if (GUILayout.Button ("RedMine",GUILayout.Width(75)))
							{
								EditorUtility.OpenWithDefaultApp(path.link);
							}
						}
						else
						{
							GUILayout.Button (" ",GUILayout.Width(75));
						}
						GUILayout.EndHorizontal();
					}
					else
					{
						GUILayout.EndHorizontal();
					}
				}
			}
			EditorGUILayout.EndScrollView();
		}

		void OpenBrowser()
		{
			_MofaLibPath = EditorUtility.OpenFolderPanel ("MofaLib Pacakge Browser", _MofaLibPath, "mofa_u3dlib");
			File.WriteAllText(Application.dataPath+@"\mofalibConfig.txt",_MofaLibPath);
		}

		void SearchForPackages()
		{
			_packagesPaths.Clear ();
			string[] dirPaths = Directory.GetFiles(_MofaLibPath,"*.unitypackage" ,SearchOption.AllDirectories);
			foreach(string path in dirPaths)
			{
				string aPath = path.Replace(@"\","/");
				string[] newPathName = aPath.Split('/');
				string[] fullName = newPathName[newPathName.Length-1].Split('.');
				MofalibPaths pathAndName = new MofalibPaths();
				if(aPath.Contains("editorAssets"))
				{
					pathAndName.category = "Editor Asset";
				}
				else
				{
					pathAndName.category = newPathName[newPathName.Length-2];
				}
				string previewImage = aPath.Replace("unitypackage","png");
				if(File.Exists(previewImage))
				{
					pathAndName.preview = previewImage;
				}
				string redMineLink = aPath.Replace("unitypackage","url");
				if(File.Exists(redMineLink))
				{
					pathAndName.link = redMineLink;
				}
				string videoLink = aPath.Replace(".unitypackage","_Video.url");
				if(File.Exists(videoLink))
				{
					pathAndName.videoLink = videoLink;
				}

				string theFolderPath = path.Remove(path.Length - newPathName[newPathName.Length-1].Length-1);
				theFolderPath = theFolderPath.Replace("/",@"\");
				pathAndName.folderPath = theFolderPath;
				pathAndName.path = aPath;
				pathAndName.name = fullName[0];
				if(!pathAndName.name.ToLower().Contains("mofalib browser"))
				{
					_packagesPaths.Add (pathAndName);
				}
			}
			_packagesPaths.Sort ();
		}

		void GetHelpFileLink()
		{
			string[] helpFiles = Directory.GetFiles(_MofaLibPath,"MofaLib Guide.url");
			_helpFile = helpFiles[0].Replace(@"\","/");
		}

		void GetDriveVideoLocationLink()
		{
			string[] videoLocation = Directory.GetFiles(_MofaLibPath,"DriveVideoPreview.url");
			_googleDriveVideos = videoLocation[0].Replace(@"\","/");
		}

		public static void ShowDisplayDialog(string title, string message)
		{
			EditorUtility.DisplayDialog (title, message, "ok");
		}
	}


}
	
public class MofaLib_PreviewWindow : EditorWindow 
{
	static Texture2D image;

	public static void ShowPreviewWindow()
	{
		MofaLib_PreviewWindow window = (MofaLib_PreviewWindow)EditorWindow.GetWindow(typeof(MofaLib_PreviewWindow));
		window.title = "Preview";
	}

	void OnGUI()
	{
		//if(image != null)
		//{
			GUILayout.Label(image);
		//}
	}

	public static void LoadPreviewImage(string url)
	{
		WWW www = new WWW("file://"+ url);
		for (float t = 0; t <= 1; t+= Time.deltaTime)
		{
			if(www.isDone)
			{
				break;
			}
		}


		image = new Texture2D (www.texture.width, www.texture.height);
		www.LoadImageIntoTexture (image);
	}
}
#endif
