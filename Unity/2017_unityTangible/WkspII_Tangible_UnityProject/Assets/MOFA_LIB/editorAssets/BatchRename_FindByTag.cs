﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

namespace MofaLib
{

	public class BatchRename_FindByTag : EditorWindow {

		static BatchRename_FindByTag window;
		string tagValue="";

		bool init = false;
		int nameZeros = 1 ;
		string newName ="Enter new name here";
		Vector2 scrollValue = Vector2.zero;
		List<GameObject> searchResult = new List<GameObject> ();
		 
		[MenuItem("MofaLib/Tools/BatchRename FindByTag")]
	
		static void OpenTagSearcher()
		{
			window = (BatchRename_FindByTag)EditorWindow.GetWindow (typeof (BatchRename_FindByTag));
			window.title = "BatchRename";
			window.minSize = new Vector2 (400, 200);
		}
 
		void OnGUI()
		{
			if(!init)
			{
				LookForExistingSelection();
				init = true;
			}
			tagValue = EditorGUILayout.TagField("Choose tag",tagValue,GUILayout.Width(350));

			GUILayout.BeginHorizontal();
			if (GUILayout.Button ("Select by Tag",GUILayout.Height(30),GUILayout.Width(100)))
			{
				SelectObjects();
			}
			if (GUILayout.Button ("Use Current Selection",GUILayout.Height(30),GUILayout.Width(150)))
			{
				LookForExistingSelection();
			}
			if (GUILayout.Button ("Clear Selection",GUILayout.Height(30),GUILayout.Width(100)))
			{
				searchResult.Clear();
				Selection.activeObject = null;
			}
			GUILayout.EndHorizontal();
			GUILayout.Label ("");		
			GUILayout.Label ("Rename with increment");
			GUILayout.BeginHorizontal();
			nameZeros = EditorGUILayout.IntField ("Number of zeros", nameZeros,GUILayout.Width(200));
			GUILayout.EndHorizontal();
			newName = GUILayout.TextField (newName,GUILayout.Width(400));
			if (GUILayout.Button ("Rename",GUILayout.Width(100)))
			{
				RenameWithIncrement();
			}
			scrollValue = EditorGUILayout.BeginScrollView(scrollValue);
 
    		if(searchResult.Count> 0)
    		{
				GUILayout.Label("Selection List");
      			foreach(GameObject obj in searchResult)
      			{
					if(obj !=null)
         			{
             			if(GUILayout.Button(obj.name))
             			{
                			Selection.activeObject =  obj;
                			EditorGUIUtility.PingObject(obj);
             			} 
         			}
         		}
  			}
			EditorGUILayout.EndScrollView();
  		}

		void SelectObjects()
		{
			GameObject[] searchArray = GameObject.FindGameObjectsWithTag(tagValue);
			searchResult.Clear ();
			foreach (GameObject o in searchArray)
			{
				searchResult.Add(o);
			}
			Selection.objects = searchResult.ToArray();
		}

		void RenameWithIncrement()
		{
			int i = 0;
			foreach (GameObject o in searchResult)
			{
				string digits = i.ToString();

				for (int j = digits.Length; j<nameZeros; j++)
				{
					digits = "0"+digits;
				}
				digits = "_"+digits;
				o.name = newName + digits;
				i++;
			}
		}

		void LookForExistingSelection()
		{
			if(Selection.gameObjects.Length>0)
			{
				searchResult.Clear();
				foreach(GameObject o in Selection.gameObjects)
				{

					searchResult.Add(o);
				}
			}
		}
 	}


}
#endif