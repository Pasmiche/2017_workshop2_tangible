﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreboardManager : Singleton<ScoreboardManager> {

	public ScoreboardGameState _gameStateMessage;
	public ScoreboardGameStatistics _gameStatisticsMessage;
	public ScoreboardMessage _gameMessage;
	public AsyncMqtt _mqttManager;

	public bool _exportJson;

	// Use this for initialization
	void Start () 
	{
		_gameStatisticsMessage = null;

		if(_exportJson && Application.isEditor)
		{
			_gameStateMessage.SaveJson("ScoreboardState.json");
			_gameStatisticsMessage.SaveJson("ScoreboardStatistics.json");
			_gameMessage.SaveJson("ScoreboardMessage.json");
		}
	}

	public void SendGamePresentation(ScoreboardGameState.ScoreboardTeam[] teams, int minScore, int maxScore)
	{
		_gameStateMessage = new ScoreboardGameState(DateTime.Now.DateTimeToEpoch(), teams, minScore, maxScore);
		_gameStateMessage.SetGameMode(0);
		SendGameState();
	}

	public void SendGameStart()
	{
		_gameStateMessage.timestamp = DateTime.Now.DateTimeToEpoch();
		_gameStateMessage.SetGameMode(1);
		SendGameState();
	}

	public void SendScore(int[] scores)
	{
		for(int i = 0; i< scores.Length; i++)
		{
			_gameStateMessage.teams[i].score = scores[i];
		}
		SendGameState();
	}

	public void SendGameMessage(string topMessage, string bottomMessage, Color messageColor)
	{
		_gameMessage = new ScoreboardMessage(DateTime.Now.DateTimeToEpoch(), topMessage, bottomMessage, messageColor);
		string json = JsonUtility.ToJson(_gameMessage);
		_mqttManager.sendMqttMsgToTopic("connectedfactory/nowhere/scoreboard/gamemessage", json);
	}

	public void UpdateGameStats(string statName, string[] teamNames, string[] statVal)
	{
		if(_gameStatisticsMessage == null)
		{
			_gameStatisticsMessage = new ScoreboardGameStatistics();
		}

		if(_gameStatisticsMessage.gameStats == null)
		{
			_gameStatisticsMessage.gameStats = new List<ScoreboardGameStatistics.ScoreboardStat>();
		}

		if(_gameStatisticsMessage.gameStats.Count <= 0)
		{
			ScoreboardGameStatistics.ScoreboardStat stat = new ScoreboardGameStatistics.ScoreboardStat(statName, teamNames, statVal);
			_gameStatisticsMessage.gameStats.Add(stat);
		}
		else
		{
			bool containsStats = false;
			for(int i=0; i< _gameStatisticsMessage.gameStats.Count; i++)
			{
				if(_gameStatisticsMessage.gameStats[i].statName ==  statName)
				{
					_gameStatisticsMessage.gameStats[i].UpdateStatsValues(statVal);
					containsStats = true;
				}
			}
			if(!containsStats)
			{
				ScoreboardGameStatistics.ScoreboardStat stat = new ScoreboardGameStatistics.ScoreboardStat(statName, teamNames, statVal);
				_gameStatisticsMessage.gameStats.Add(stat);
			}
		}
		SendGameStatistics();
	}

	public void SendGameStatistics()
	{
		_gameStatisticsMessage.timestamp = DateTime.Now.DateTimeToEpoch();
		string json = JsonUtility.ToJson(_gameStatisticsMessage);
		_mqttManager.sendMqttMsgToTopic("connectedfactory/nowhere/scoreboard/gamestatistics", json);
	}

	public void SendGameEnd(int winningTeam)
	{
		_gameStateMessage.winningTeam = winningTeam;
		SendGameState();
	}

	void SendGameState()
	{
		string json = JsonUtility.ToJson(_gameStateMessage);
		_mqttManager.sendMqttMsgToTopic("connectedfactory/nowhere/scoreboard/gamestate", json);
	}

	public void ResetGame()
	{
		_gameStateMessage.timestamp = DateTime.Now.DateTimeToEpoch();
		_gameStateMessage.SetGameMode(3);
		SendGameState();
	}
}

// game start
[Serializable]
public class ScoreboardGameState
{
	public double timestamp;
	public int minScore;
	public int maxScore;
	public enum ScoreboardMode {toPresentation, toGame, toWinner, reset};
	public ScoreboardMode mode;
	public int winningTeam;
	public ScoreboardTeam[] teams;

	[Serializable]
	public class ScoreboardTeam
	{
		public string teamName;
		public Color teamColor;
		public int score;
		public ScoreboardPlayer[] teamPlayer;
	}

	[Serializable]
	public class ScoreboardPlayer
	{
		public string username;
		public string participant_id;
		
		public ScoreboardPlayer(string playerName, string playerId)
		{
			username = playerName;
			participant_id = playerId;
		}
	}

	public ScoreboardGameState(double timeStamp, ScoreboardTeam[] teamArray, int minimumScore, int maximumScore)
	{
		timestamp = timeStamp;
		teams = teamArray;
		minScore = minimumScore; 
		maxScore = maximumScore;
	}

	public void SetGameMode(int boardMode)
	{
		mode = (ScoreboardMode)boardMode;
	}
}

// game stats
[Serializable]
public class ScoreboardGameStatistics
{
	public double timestamp;
	public List<ScoreboardStat> gameStats;

	[Serializable]
	public class ScoreboardStat
	{
		public string statName;
		public ScoreboardTeamStat[] statVals;

		public ScoreboardStat(string name, string[] teams, string[] vals)
		{
			statName = name;
			statVals = new ScoreboardTeamStat[teams.Length];
			for(int i = 0; i< statVals.Length; i++)
			{
				statVals[i] = new ScoreboardTeamStat();
				statVals[i].teamName = teams[i];
				statVals[i].statVal = vals[i];
			}
		}

		public void UpdateStatsValues(string[] vals)
		{
			for(int i = 0; i< statVals.Length; i++)
			{
				statVals[i].statVal = vals[i];
			}
		}
	}

	[Serializable]
	public class ScoreboardTeamStat
	{
		public string teamName;
		public string statVal;
	}

	
}

// game message
[Serializable]
public class ScoreboardMessage
{	
	public GameMessage message;

	[Serializable]
	public class GameMessage
	{
		public double timestamp;
		public string upperMessage;
		public string lowerMessage;
		public Color color;
	}

	public ScoreboardMessage(double timeStamp, string topMessage, string bottomMessage, Color messageColor)
	{
		message = new GameMessage();
		message.timestamp = timeStamp;
		message.upperMessage = topMessage;
		message.lowerMessage = bottomMessage;
		message.color = messageColor;
	}
}



